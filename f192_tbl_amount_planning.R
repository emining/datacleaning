### f193_cal_tbl_size
### 각 테이블의 컬럼의 용량을 산정해서 테이블의 용량을 계산함
###
### input
### - 테이블 정의서의 컬럼 리스트
### - 테이블 정의서의 테이블 리스트
###
### output
### - 테이블 정의서의 테이블 리스트에 있는 테이블 별 용량
### - no 
### - ASIS account
### - ASIS table name
### - TOBE table name
### - ASIS table record account
### - table length( sum of column length)
### - table volumn( account * length )
### 
### Algorithm
### 1. TOBE 모델의 각 테이블 별로 ASIS에서 데이터를 카운트 함
### 2. TOBE 모델의 각 테이블 별로 길이를 구함
### 3. ASIS table의 acc와 tbl_nm_ap 로 tbl_nm_tp와 매칭을 해서 카운트를 할당한다.
### 4. 곱해서 볼륨을 구한다
###
###
print('f192 테이블 용량 설계 시작')

### Preprocess
rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

input_tbl_nm <- inputAppTblName
input_col_nm <- inputAppColName
input_cnt_nm <- '../output/o110_tbl_info_pmissis.csv'

outputName <- '../output/o192_tbl_amount.csv'
if(file.exists(outputName)) file.remove(outputName)

app_tbl_df <- fReadPureUtf8Tbl(input_tbl_nm)
app_col_df <- fReadPureUtf8Tbl(input_col_nm)
app_col_df <- subset(app_col_df, subset = (col_use != 0))

cnt_df <-  fReadPureUtf8Tbl(input_cnt_nm)
tobe_tbl_df <- subset(app_tbl_df, select = c(no, acc, tbl_nm_ap, tbl_nm_tp, tbl_nm_tl ) , 
    subset = (tbl_use != 0))

nr_tbl <- nrow(app_tbl_df)
nr_col <- nrow(app_col_df)
nr_cnt <- nrow(cnt_df)
nr_tobe <- nrow(tobe_tbl_df)

output_df <- fMakePureDF(0,nr_tobe, 8)
names(output_df) <- c('no','acc', 'tbl_nm_ap', 'tbl_nm_tp', 'tbl_nm_tl', 'count','tbl_len', 'volumn')
nr_output <- nrow(output_df)

## 1. TOBE 모델의 각 테이블 별로 ASIS에서 데이터를 카운트 함
for(i in 1:nr_tobe){
  
  output_df$no[i] <- tobe_tbl_df$no[i]
  output_df$acc[i] <- tobe_tbl_df$acc[i]
  output_df$tbl_nm_ap[i] <- tobe_tbl_df$tbl_nm_ap[i]
  output_df$tbl_nm_tp[i] <- tobe_tbl_df$tbl_nm_tp[i]
  output_df$tbl_nm_tl[i] <- tobe_tbl_df$tbl_nm_tl[i]

  asisTblName <- tobe_tbl_df$tbl_nm_ap[i]
  asisAcc <- tobe_tbl_df$acc[i]
  
  rlt <- subset(cnt_df, select = count, subset = (user == asisAcc & table == asisTblName ))
  rltCo <- nrow(rlt)
  
  if(rltCo == 1){

    countMatch <- rlt[1,1]
    #print(countMatch)
    output_df$count[i] <- countMatch

  }else if(rltCo > 1){

    print(rlt) 
    stop('double')
  
  }else{
    
    output_df$count[i] <- 0
    #print('not match')
  
  }
}

### 2. TOBE 모델의 각 테이블 별로 길이를 구함
for(i in 1:nr_output){

  tblNameTP1 <- output_df$tbl_nm_tp[i]

  for(j in 1:nr_col){

    tblNameTP2 <- app_col_df$tbl_nm_tp[j]

    if(tblNameTP1 == tblNameTP2)

    if(!is.na(app_col_df$len_t[j])){
    output_df$tbl_len[i] <- output_df$tbl_len[i] + app_col_df$len_t[j]
    }
  }
}

### 4. 곱해서 볼륨을 구한다

for( i in 1:nr_output){

  output_df$volumn[i] = output_df$count[i] * output_df$tbl_len[i]

}

fWritePureUtf8Tbl(output_df, outputName)

print('f192 테이블 용량 끝')
