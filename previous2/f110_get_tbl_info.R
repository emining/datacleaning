### a01 - 유저 테이블 정보 분석
### 
### output
### 모든 유저-테이블 리스트 및 각 테이블 별 데이터 수
### 유저-테이블 중복 검사
### ownerAndTbl은 일단 필터링을 하기 전의 전체 테이블이며, 여기서 
### 비번이 없거나 분석이 필요없는 계정은 제외해서 분석 리스트를 
### 각 필터링된 테이블 별 데이터 수 조회

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

serverIP = '20dev'
connectInfo <- fSetIp(serverIP)
id <- connectInfo[1]
pw <- connectInfo[2]
sid <- connectInfo[3]
ip <- connectInfo[4]
port <- connectInfo[5]

## 테이블 기본 정보 받아오기  
conn <- fDBconnect(ip,port,sid,id,pw)
queryResult<- fDBgetQuery(conn,"SELECT * FROM all_tables")
dbDisconnect(conn)

countTbl <- nrow(queryResult)
countCol <- ncol(queryResult)
colNames <- colnames(queryResult)
ownersDup <- queryResult[,1]
owners <- as.data.frame(unique(ownersDup))
ownerAndTbl <- queryResult[,1:2]
fCheckDupRaw(ownerAndTbl) # 테이블 중복 검사

## output data.frame 작성
## idx, user, table, count
tblTbl <- fMakePureDF('', 0, 4) # nrow가 0인게 맞나?? 
oneTblInfo <- fMakePureDF('',1,4)

names(tblTbl) <- c('idx', 'user', 'table', 'count')
names(oneTblInfo) <- c('idx', 'user', 'table', 'count')


## 필요없는 유저 필터링을 한다.
nR <- nrow(ownerAndTbl)
idx <- 0
colCount <- 0

if(serverIP == 18) idxAdd <- 10000
if(serverIP == 20) idxAdd <- 20000


for( i in 1:nR){
    id <-ownerAndTbl[i,1]
    tblName <-str_trim(ownerAndTbl[i,2])
    pw <- fGetPw(sid,id,userTbl)
    pw <- as.character(pw)
    # 불필요한 계정은 패스
    if(id == 'SYS') next
    if(id == 'SYSTEM') next
    if(id=='SCOTT') next
    if(id=='MDSYS') next
    if(id=='LBACSYS') next
    if(id=='DVSYS') next
    if(id=='CTXSYS') next
    if(id=='OJVMSYS') next
    if(id=='WMSYS') next
    if(id=='XDB') next
    if(id=='GSMADMIN_INTERNAL') next
    if(id=='APPQOSSYS') next
    if(id=='OUTLN') next
    if(id=='APEX_040200') next
    if(id=='AUDSYS') next
    if(id=='ORDSYS') next
    if(id=='ORDDATA') next
    if(id=='FLOWS_FILES') next
    if(id=='OLAPSYS') next
    if(id=='DEVDBA') next

#    if(id == "DBSFWUSER") browser()

    if(length(pw)==0){
        print(paste(sid,'-',id,'has no paswd')) 
    } else{
        idx <- idx + 1

        ## ip 별로 예외상황 설정
        if(serverIP==18){
            #
        }else if(serverIP==20){
            #if(tblName == 'MBTX_TONSE_INCOME') next
        }
        ## 작업 시작
        ## 테이블과 테이블의 카운트를 조회한다. 
        print(paste((idx+idxAdd),'a01::', serverIP, 'user::', id, 
            ':: table::',  tblName, 'start'))
        conn <- fDBconnect(ip,port,sid,id,pw)
        querySentence2 <- paste('SELECT COUNT(*)  FROM' , tblName)
        getQuery2 <- fDBgetQuery(conn, querySentence2)
        dbDisconnect(conn)    
        rowCount <- getQuery2[,1]
        print(paste((idx+idxAdd),'a01::', serverIP, 'user::', id, 
            ':: table::',  tblName, 'count',rowCount ))
        
        oneTblInfo[1,1] <- idx+idxAdd
        oneTblInfo[1,2] <- id
        oneTblInfo[1,3] <- tblName
        oneTblInfo[1,4] <- rowCount
        tblTbl <- rbind(tblTbl, oneTblInfo)
    }                
}

### 저장
if(serverIP == 18){
    outputName <- '../output/o110_tbl_info_ptold.csv'
}else if(serverIP == '20dev'){
    outputName <- '../output/o110_tbl_info_pmis2dev.csv'
}else{
  outputName <- '../output/o110_tbl_info_pmissis.csv'
}
write.csv(tblTbl,outputName,row.names=FALSE)
print(paste('f110::',serverIP,'finished'))
