### a02 - 테이블 스키마를 불러옴
###

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

serverIP = 20
connectInfo <- fSetIp(serverIP)
id <- connectInfo[1]
pw <- connectInfo[2]
sid <- connectInfo[3]
ip <- connectInfo[4]
port <- connectInfo[5]

## 전처리

## p01 테이블 기본 정보 받아오기  
conn <- fDBconnect(ip,port,sid,id,pw)
queryResult<- fDBgetQuery(conn,"SELECT * FROM all_tables")
dbDisconnect(conn)

countTbl <- nrow(queryResult)
countCol <- ncol(queryResult)
colNames <- colnames(queryResult)
ownersDup <- queryResult[,1]
owners <- as.data.frame(unique(ownersDup))
ownerAndTbl <- queryResult[,1:2]
fCheckDupRaw(ownerAndTbl) # 테이블 중복 검사(이것, a01에서 작업을 했는데 필요한가???)

nR <- nrow(ownerAndTbl)
idx <- 0
colCount <- 0

for( i in 1:nR){
    id <-ownerAndTbl[i,1]
    tblName <-str_trim(ownerAndTbl[i,2])
    pw <- fGetPw(sid,id,userTbl)
    pw <- as.character(pw)
    # 불필요한 계정은 패스
    if(id == 'SYS') next
    if(id == 'SYSTEM') next
    if(id=='SCOTT') next
    if(id=='MDSYS') next
    if(id=='LBACSYS') next
    if(id=='DVSYS') next
    if(id=='CTXSYS') next
    if(id=='OJVMSYS') next
    if(id=='WMSYS') next
    if(id=='XDB') next
    if(id=='GSMADMIN_INTERNAL') next
    if(id=='APPQOSSYS') next
    if(id=='OUTLN') next
    if(id=='APEX_040200') next
    if(id=='AUDSYS') next
    if(id=='ORDSYS') next
    if(id=='ORDDATA') next
    if(id=='FLOWS_FILES') next
    if(id=='OLAPSYS') next
    if(tblName == 'PTB_SSOP7103') next
    if(tblName == 'PTB_SSOP3201') next
    if(tblName == '1150 PTB_SSOP0006_H') next

    if(length(pw)==0) {
        print(paste(sid,'-',id,'has no paswd')) 
    } else {
        idx <- idx + 1
    
        ## ip 별로 예외상황 설정
        if(serverIP==18){
            #
        }else if(serverIP==20){
            #if(tblName == 'MBBS_ORG') next #BLOB
            #if(tblName == 'MBRP_DECL_D') next
            #if(tblName == 'MBTX_NAV_VESSEL') next
            #if(tblName == 'MBTX_VESSEL_STD') next
            #if(tblName == 'TC_ORG') next
            #if(tblName == 'MBTX_TONSE_INCOME') next
            
        }
        
        ## 작업 시작
        print(paste('a02:::user::', id,'-table::',(idx+10000),
            tblName,'start' ))
        ## 테이블의 컬럼 구조정보 가지고 오기
        conn <- fDBconnect(ip,port,sid,id,pw)
        querySentence <- paste('select COLUMN_NAME, DATA_TYPE, 
            DATA_LENGTH, NULLABLE from USER_TAB_COLUMNS 
            where TABLE_NAME=\'', tblName, '\' order by column_id', 
            sep="")
        
        #print(querySentence)
        #querySentence <- paste('select * from', tblName)
        getQuery <- fDBgetQuery(conn, querySentence)
        querySentence2 <- paste('SELECT COUNT(*)  FROM' , tblName)
        getQuery2 <- fDBgetQuery(conn, querySentence2)
        dbDisconnect(conn)
        rowCount <- getQuery2[,1]

        colCount <- colCount + nrow(getQuery)
        if(nrow(getQuery)!=0 ){
            
            idTbl <- fMakePureDF(id, nrow(getQuery), 1 )
            names(idTbl) <- 'userID'


            #countTbl <- data.frame(matrix(rowCount,nrow=nrow(getQuery), 
            #    ncol = 1))
            countTbl <- fMakePureDF(rowCount, nrow(getQuery), 1)
            names(countTbl) <- 'count'

            tblTbl <- fMakePureDF(tblName, nrow(getQuery), 1 )
            #tblTbl <-data.frame(matrix(tblName,nrow=nrow(getQuery), 
            #    ncol = 1))
            names(tblTbl) <- 'tblName'
            rltTbl <-cbind(idTbl,tblName,getQuery,countTbl)
        }
        
        if(idx == 1){
            totalTbl <- rltTbl
        }else{
            totalTbl <- rbind(totalTbl,rltTbl)
        }

        # if(idx== 5) break
        ## 작업 끝
    }
}

### 인덱스 번호 추가 후 합치기
nR <- nrow(totalTbl)
if(serverIP == 18){
    idxTbl <- data.frame(matrix(100001:(100000+nR),nrow=nR,ncol=1))
}else{
    idxTbl <- data.frame(matrix(200001:(200000+nR),nrow=nR,ncol=1))
}
names(idxTbl) <- 'idx'

totalTbl <-cbind(idxTbl,totalTbl)

### 저장
if(serverIP == 18){
    outputName <- 'output/o120_schemaTbl_ptold.csv'
}else{
    outputName <- 'output/o120_schemaTbl_pmissis.csv'
}
write.csv(totalTbl,outputName,row.names=FALSE)
print('f120 finished')
