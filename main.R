"no",
"acc",
"tbl_nm_ap",
"no1",
"col_nm",
"col_nm_ap",
"type",
"len",
"char1",
"pk_a",
"nn1",
"col_use",
"acc_t",
"tbl_nm_tl",
"tbl_nm_tp",
"no2",
"col_nm_tl",
"col_nm_tp",
"type2",
"len_t",
"char2",
"pk2",
"nn2",
"uk2"

"no",
"acc",
"tbl_nm_ap",
"no1",
"col_nm",
"col_nm_ap",
"type",
"len",
"char1",
"pk_a",
"nn1",
"col_use",
"acc_t",
"tbl_nm_tl",
"tbl_nm_tp",
"no2",
"col_nm_tl",
"col_nm_tp",
"type2",
"len_t",
"char2",
"pk2",
"nn2",
"uk2"

"no",
"acc",
"tbl_nm_ap",
"no1",
"col_nm",
"col_nm_ap",
"type",
"len",
"char1",
"pk_a",
"nn1",
"col_use",
"acc_t",
"tbl_nm_tl",
"tbl_nm_tp",
"no2",
"col_nm_tl",
"col_nm_tp",
"type2",
"len_t",
"char2",
"pk2",
"nn2",
"uk2"

### 공통라이브러리 로딩
# if(file.exists(outputName)) file.remove(outputName)

### 테이블 리스트를 DB에서 받아오는 스크립트
# source('fgettblinfofromdb.R', encoding = 'UTF-8')

### DBA계정을 이용해서 일괄적으로 전체DB의 테이블(sys포함)받아오는 방식
# source('fgetcolinfofromdb.R', encoding = 'UTF-8')

### 컬럼 데이터를 csv로 저장
#source('fcol2csv.R', encoding = 'UTF-8') 

## 테이블 정의서의 AS-IS 테이블 중 TO-BE로 이관되는 테이블이 DB에 있는지 체크
#source('f140_chk_asis_tbl_exist.R', encoding = 'UTF-8') 

## 테이블정의서의 TOBE모델에 사용하는 컬럼의 테이블의 asis명이 테이블 리스트에 없거나 
## 사용안함의 경우를 발견 
#source('f141_chk_asis_tbl_col_tblname.R',encoding = 'UTF-8') 

## 테이블정의서의 TOBE모델에 사용하는 컬럼의 테이블의 TOBE명이 테이블 리스트에 없거나 
## 사용안함의 경우를 발견 
#source('f142_chk_tobe_tbl_col_tblname.R',encoding = 'UTF-8') 

## 테이블 정의서의 TOBE 컬럼이 신규인지, 이관인지 등의 출처인텍스 exist를 붙이는 기능S
#source('f150_chk_col_exist.R',encoding = 'UTF-8') 

## TOBE 모델의 테이블의 논리 및 물리명의 중복을 체크함(유니크성 체크)
#source('f161_chk_tobe_tbl_name_dup.R',encoding = 'UTF-8') 

## TOBE 모델의 컬럼의 논리 및 물리명의 중복을 체크함(유니크성 체크)
#source('f162_chk_tobe_col_name_dup.R',encoding = 'UTF-8') 

###To-Be 모델의 테이블 및 컬럼명 중복을 확인
#source('f170_chk_tobe_schema_dup.R',encoding = 'UTF-8')

## source('f180_pmissis_chk_length_.R',encoding = 'UTF-8')

## 테이블정의서 작성(input - o140,150) 
#source('f190_make_table_def_file.R',encoding = 'UTF-8')

#source('f191_chk_col.R',encoding = 'UTF-8') #filename check

###ASIS DB 빈도분석(input - o120, 150)
#source('f251_freq_anal_by_app_col2.R',encoding = 'UTF-8')

#source('f310_create_tbl.R',encoding = 'UTF-8')


## 파라미터는 아래와 같음
## getcol - DB정보를 얻어서 그것으로 컬럼정보를 추출
## dbscript - 테이블 설계서를 바탕으로 테이블 정의서 도출

para <- "dbscript"

if(para == "getcol"){

    source('fgettblinfofromdb.R', encoding = 'UTF-8')
    source('fgetcolinfofromdb.R', encoding = 'UTF-8')
    source('fcol2csv.R', encoding = 'UTF-8') 

}else if(para == "dbscript"){

    
    
}


