### 컬럼별 유사도 분석
### 특정 컬럼에 대해서 부분집합과 유사한 컬럼을 찾는다.
### 우선 부분집합이 있는지를 찾고, 두번째로는 유사한 컬럼을 찾는다.
### 비교대상이 되는 컬럼은 csv로 읽어들이거나, 혹은 컬럼 번호를 지정해서 찾는다
### 우선 해운종합 내에서만 한다. 
### 검색 결과
### 


rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

autoRun_df <- data.frame(matrix(0, nrow=2, ncol=2))

#autoRun_df[1,] <- c(1,1)
#autoRun_df[2,] <- c(1,2)
#autoRun_df[3,] <- c(2,1)
#autoRun_df[4,] <- c(2,2)
#autoRun_df[5,] <- c(2,3)
#autoRun_df[6,] <- c(2,4)
#autoRun_df[7,] <- c(3,1)
#autoRun_df[8,] <- c(3,2)
#autoRun_df[9,] <- c(3,3)
#autoRun_df[10,] <- c(3,4)
#autoRun_df[11,] <- c(4,1)
#autoRun_df[12,] <- c(4,2)
#autoRun_df[13,] <- c(5,1)
#autoRun_df[14,] <- c(5,2)
#autoRun_df[15,] <- c(6,1)
#autoRun_df[16,] <- c(6,2)
#autoRun_df[17,] <- c(6,3)
#autoRun_df[18,] <- c(6,4)
#autoRun_df[19,] <- c(7,1)
#autoRun_df[20,] <- c(7,2)
#autoRun_df[21,] <- c(7,3)
#autoRun_df[22,] <- c(7,4)
#autoRun_df[1,] <- c(8,1)
#autoRun_df[2,] <- c(8,2)
autoRun_df[1,] <- c(9,1)
autoRun_df[2,] <- c(9,2)



searchMode <- 0 # 0 - 파일 지정, 0이 아니면  컬럼번호 임

for(idxAuto in 1:nrow(autoRun_df)){
  searchObjNum <- autoRun_df[idxAuto,1]
  varNameNum <- autoRun_df[idxAuto,2]
  
  
  if(searchObjNum == 1){
    searchObj <- 'upjong_code' 
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name'
    else stop('varName is wrong')
    
  }else if(searchObjNum == 2){
    searchObj <- 'chung_code'
    
    if(varNameNum == 1) varName <- 'local'
    else if(varNameNum == 2) varName <- 'spot'
    else if(varNameNum == 3) varName <- 'numcode'
    else if(varNameNum == 4) varName <- 'encode'
    else stop('varName is wrong')
    
  }else if(searchObjNum == 3){
    searchObj <- 'nation_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name_en'
    else if(varNameNum == 3) varName <- 'name_kr'
    else if(varNameNum == 4) varName <- 'local_code'
    else stop('varName is wrong')
    
  }else if(searchObjNum == 4){
    searchObj <- 'morae_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'place'
    else stop('varName is wrong')
    
    
  }else if(searchObjNum == 5){
    searchObj <- 'entrance_object_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name'
    else stop('varName is wrong')
    
  }else if(searchObjNum == 6){
    searchObj <- 'ship_type_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name'
    else if(varNameNum == 3) varName <- 'statcode'
    else if(varNameNum == 4) varName <- 'statname'
    else stop('varName is wrong')
  
    }else if(searchObjNum == 7){
    searchObj <- 'harbor_code'
    
    if(varNameNum == 1) varName <- 'harbor_name'
    else if(varNameNum == 2) varName <- 'cd'
    else if(varNameNum == 3) varName <- 'code_num'
    else if(varNameNum == 4) varName <- 'code_en'
    else stop('varName is wrong')

    }else if(searchObjNum == 8){
    searchObj <- 'harbor_use_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name'
    else stop('varName is wrong')

    }else if(searchObjNum == 9){
    searchObj <- 'sail_purpose_code'
    
    if(varNameNum == 1) varName <- 'code'
    else if(varNameNum == 2) varName <- 'name'
    else stop('varName is wrong')



  }else{  
    stop('search object number is wrong')
  }
  
  sameCol <- NULL
  subsetCol <- NULL
  
  if(searchMode == 0){
    if(searchObj == 'upjong_code'){
      input_df <- read.csv("code/01_upjong_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'chung_code'){
      input_df <- read.csv("code/02_chung_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'nation_code'){
      input_df <- read.csv("code/03_nation_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'morae_code'){
      input_df <- read.csv("code/04_morae_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'entrance_object_code'){
      input_df <- read.csv("code/05_entrance_object_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'ship_type_code'){
      input_df <- read.csv("code/06_ship_type_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
      
    }else if(searchObj == 'harbor_code'){
      input_df <- read.csv("code/07_harbor_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]

    }else if(searchObj == 'harbor_use_code'){
      input_df <- read.csv("code/08_harbor_use_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]

    }else if(searchObj == 'sail_purpose_code'){
      input_df <- read.csv("code/09_sail_purpose_code.csv",stringsAsFactors=F,strip.white=TRUE)
      input <- input_df[varName]
    

    }else{ 
      inputFileName <- paste('output/a03_coldata/', searchMode, '.csv', sep="")
      input <- read.csv(inputFileName,stringsAsFactors=F,strip.white=TRUE)
    }
  }
  
  if(is.null(input)) stop('input is wrong')
  
  ## col data loading
  colList_df <- read.csv("output/a05_chk_col_pmissis.csv", stringsAsFactors=F,strip.white=TRUE )
  nCol <- nrow(colList_df)
  idx_mt <- matrix(0, nrow = nCol, ncol = 1)
  dbColList_df <- read.csv("output/a02_schemaTbl_pmissis.csv", stringsAsFactors=F,strip.white=TRUE )
  
  for(i in 1:nCol){
    fDispLoof(i,100,nCol)
    if(colList_df$exist[i] != 1) next
    if(colList_df$Data_Type[i] == "BLOB") next
    if(colList_df$Data_Type[i] == "CLOB") next
    
    colNum <- colList_df$DB_colNumber[i]
    dataCount_df <- subset(dbColList_df, select = count, idx == colNum )
    
    
    if(dataCount_df[1,1] != 0){
      inputColName <- paste('output/a03_coldata/', colNum, '.csv', sep="")
      input2 <- read.csv(inputColName, stringsAsFactors=F,strip.white=TRUE)
      input2 <- input2[,1]
      
      #remove NA
      input2 <- input2[!is.na(input2)]
      input2 <- unique(input2)
      
      #if(i ==  55) browser()    
      if(length(input2 > 0)){
        if(setequal(input[,1],input2)==TRUE){
          sameCol <- c(sameCol, colNum)
          idx_mt[i,1] <- 1
        }
        ## check subset
        if(all(is.element(input2, input[,1])) == TRUE){
          subsetCol <- c(subsetCol, colNum) 
          idx_mt[i,1] <- 1
          
        }    
      }
    }
  }
  
  rlt_df <- colList_df
  idx_mt_df <- data.frame(idx_mt)
  rlt_df <- cbind(rlt_df,idx_mt_df)
  rlt_final <- rlt_df[rlt_df$idx_mt == 1,]
  
    
  if(searchMode == 0){
    outputFileName <- paste('output/b05_setanal/b05_setanal_', (idxAuto+100), '_' , searchObj, '_', varName,'.csv',sep="")
    write.csv(rlt_final, outputFileName)
  }else{ 
    stop('not yet')
  }
}

