### 테이블 생성 스크립트 작성
### 입력데이터
### 컬럼 정의서csv 파일
### 작업이력
### 180103 작성 시작 
###
### 180115 - PK identifier is added to output script
###     

### 전처리
rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

## 입력파일 로딩
inputColName <- '../output/o190_tobe_col.csv'

inputCol <- fReadPureUtf8Tbl(inputColName)

outputSql <- data.frame(matrix('', nrow = 30000, ncol = 1), stringsAsFactors=F)
nR <- nrow(inputCol)

idxRow <- 0

for(i in 1:nR){
  if(i == 1) isNewTbl <- 1
  else if(inputCol$tbl_nm_tp[i-1] != inputCol$tbl_nm_tp[i] ) isNewTbl <- 1
  else isNewTbl <- 0
  
  if(isNewTbl == 1){
    tobeAcc <- inputCol$acc_t[i]
    tobeTbl <- inputCol$tbl_nm_tp[i]
    tobeTblLogic <- inputCol$tbl_nm_tl[i]
    
    #if(tobeTbl == 'PTB_SSMB0230') browser()
    
    
    ## 테이블 삭제 후 생성
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- paste('--ALTER TABLE ', tobeAcc, '.' , tobeTbl, sep = "")
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- ' --DROP PRIMARY KEY CASCADE;'
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- paste('DROP TABLE  ', tobeAcc, '.' , tobeTbl, ' CASCADE CONSTRAINTS;', sep = "")
    idxRow <- idxRow + 2
    outputSql[idxRow, 1] <- paste('CREATE TABLE ', tobeAcc, '.' , tobeTbl, sep = "")
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- '('
    
    
    ## 컬럼 생성
    unit_df <- subset(inputCol, tbl_nm_tp == tobeTbl)
    
    # 컬럼 생성
    nCol <- nrow(unit_df)
    
    pkList <- '';
    
    for(j in 1:nCol){
      
      colID <- unit_df$col_nm_tp[j]
      colFormat <- unit_df$dt_t[j]
      colLength <- unit_df$len_t[j]
      colPk <- unit_df$pk_t[j]
      
      
      idxRow <- idxRow + 1
      
      if(colPk == 'PK'){
        pkList <- paste(pkList , colID, ',')
        pkList.length <- nchar(pkList)

      }
      
      if(j != nCol) {
        if(colFormat == 'DATE'){
          outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ',', sep = "")
        }else if(colFormat == 'NUMBER'){
          tmp <- as.character(colLength)
          rlt <- grep('\\.', tmp)
          if(length(rlt) == 0){
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, '.0)', ',', sep = "")  
          }else
          {
            tmp <- sub("\\." , "," , tmp )
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', tmp, ')', ',', sep = "")
          }
          
        }else if(colFormat == 'CLOB' | colFormat == 'BLOB'){
          outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ',', sep = "")
        }else{
          outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, ' CHAR)', ',', sep = "")  
        }
        
      }else {
        if(nchar(pkList) == 0){
          if(colFormat == 'DATE'){
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, sep = "")
          }else if(colFormat == 'NUMBER'){
           
            tmp <- as.character(colLength)
            rlt <- grep('\\.', tmp)
            if(length(rlt) == 0){
              outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, '.0)', sep = "")  
            }else
            {
              tmp <- sub("\\." , "," , tmp )
              outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', tmp, ')', sep = "")
            }
            
          }else if(colFormat == 'CLOB' | colFormat == 'BLOB'){
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, sep = "")
          }else{
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, ' CHAR)', sep = "")  
          }
        }else{
          if(colFormat == 'DATE'){
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ',', sep = "")
          }else if(colFormat == 'NUMBER'){
            tmp <- as.character(colLength)
            rlt <- grep('\\.', tmp)
            if(length(rlt) == 0){
              outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, '.0)', ',', sep = "")  
            }else
            {
              tmp <- sub("\\." , "," , tmp )
              outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', tmp, ')', ',', sep = "")
            }

          }else if(colFormat == 'CLOB' | colFormat == 'BLOB'){
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ',', sep = "")
          }else{
            outputSql[idxRow, 1] <- paste('  ', colID, '  ' , colFormat, ' (', colLength, ' CHAR)', ',', sep = "")  
          }   
        }
      }
    }
    
    
    idxRow <- idxRow + 1
    if(nchar(pkList) != 0){
      pkList <- substring(pkList,1, (pkList.length-1))
      outputSql[idxRow, 1] <- paste('CONSTRAINT ',  tobeTbl, '_PK PRIMARY KEY(', pkList, ')', sep="" )
    } 
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- ');'
    idxRow <- idxRow + 2
    
    ## 코멘트
    sqoat <- '\''
    idxRow <- idxRow + 1
    outputSql[idxRow, 1] <- paste('COMMENT ON TABLE ', tobeAcc, '.' , tobeTbl, '    IS \'',  tobeTblLogic, '\';', sep = "")
    
    ## column comment
    for(j in 1:nCol){
      idxRow <- idxRow + 1
      colID <- unit_df$col_nm_tp[j]
      colFormat <- unit_df$dt_t[j]
      colComment <- unit_df$col_nm_tl[j]
      outputSql[idxRow, 1] <- paste('COMMENT ON COLUMN ', tobeAcc, '.' , tobeTbl, '.', colID, '    IS   \'',  colComment,  '\';', sep = "")
    }
    idxRow <- idxRow + 3
  }
}  
  output <- outputSql[1:idxRow,]
  names(outputSql) <- ''
  write.table(output,'../output/o310_create_table.sql', 
            row.names = FALSE, fileEncoding="UTF-8",  quote = FALSE)
  print('f310 make tbl finished')
