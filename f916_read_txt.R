rm(list = ls())
library(stringr)

conn=file("input/code/nation_code.csv",open="r")
line=readLines(conn, encoding = 'UTF-8')

text_df <- data.frame(matrix('', nrow = length(line), ncol = 5), stringsAsFactors = F)

nText <- length(line)

for (i in 1:nText){
  #print(line[i])
  text_df[i,1] <- line[i]
}
close(conn)


for(i in 1:nText){
  # 우선 국가코드 영문 2자리르 뽑아
  
  a <- text_df[i,1]
  print(a)
  
  nChar <- nchar(a)
  code1Loc <- str_locate(a,"[A-Z][A-Z]")
  
  startPnt <- code1Loc[1]
  endPnt <- code1Loc[2]
  
  code1 <- str_trim(substr(a,startPnt,endPnt))
  
  remain <- substr(a,(endPnt+1),nChar)
  remain <- str_trim(remain)
  
  nChar <- nchar(remain)
  code2Loc <- str_locate(remain, "[가-힣]")
  
  code2 <- str_trim(substr(remain,1,(code2Loc-1)))
  remain <- substr(remain, code2Loc, nChar)
  remain <- str_trim(remain)
  
  nChar <- nchar(remain)
  code3Loc <- str_locate(remain, "[A-Z]")
  code3 <- str_trim(substr(remain,1,(code3Loc-1)))
  code4 <- substr(remain,code3Loc, nChar)
  
  text_df[i,2] <- code1
  text_df[i,3] <- code2
  text_df[i,4] <- code3
  text_df[i,5] <- code4
  
}


  
