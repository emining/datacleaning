### a04_chk_tbl_exist
### 테이블 정의서의 AS-IS 테이블 중 TO-BE로 이관되는 테이블이 DB에 있는지 체크하
### 입력데이터의 table_use 변수가 1인 테이블에 대해서 tblLgc에 해당 테이블이 있는지를 확인함.
### 
###
### 입력데이터
###     a01_tbl_info - DB의 입력정보
###     pmissis_tbl -  테이블 정의서의 테이블 목록
###                     
###
### 출력데이터
###     o140_tbl_exist
###     
### 작업이력
### 171227 - 업데이트 시작
### 유저별로 중복되는 테이블이 있으므로 이를 고려해야 함.

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

## 파일 
if(sid == 'pmissis') {

  inputTblName_db <- '../output/o110_tbl_info_pmissis.csv'
  outputName <- '../output/o140_chk_tbl_pmissis.csv'

} else if(sid == 'pmislgc'){
  inputTblName_db <- '../output/o110_tbl_info_ptold.csv'
  outputName <- '../output/o140_chk_tbl_ptold.csv'
}

if(file.exists(outputName)) file.remove(outputName)

## DB 정보 읽어오기
## 테이블 정보 읽기 
tblLgc <- fReadPureUtf8Tbl(inputTblName_db)

nTblLgc <- nrow(tblLgc)


## 이관되는 테이블이 있는지를 매칭하는 테이블
# idx_exist <- data.frame(matrix(0,nrow=nTblApp, ncol = 1))
idx_exist <- fMakePureDF(0, nTblApp, 1 )
names(idx_exist) <- 'exist'

## 분석 내용의 모든 테이블이 db에 있는지를 확인해서 인덱스를 생성
for(i in 1:nTblApp){
  idx <- 0
  user_app <-app_tbl$acc[i]
  tbl_name_app_phy <- app_tbl$tbl_nm_ap[i] #asis 분석
  app_tbl_use <-app_tbl$tbl_use[i]
  
  if(app_tbl_use == 1){
    for(j in 1:nTblLgc){
      user_db <- tblLgc$user[j]            
      tbl_name_db_phy <- tblLgc$table[j]
      #if(i==23 & j == 373) browser()
      if(user_app == user_db){
        if(tbl_name_app_phy == tbl_name_db_phy){
          idx <- idx + 1
          idx_exist[i,1] <- idx
        }
      }
    }
  }else if(app_tbl_use == 2){
    idx_exist[i,1] <- 2
  }
}

##  생성된 인덱스를 분석해서 TOBE에 사용되는 테이블이 ASIS에 없으면 에러 도출 
for( i in 1:nTblApp){
  
  if(app_tbl$tbl_use[i]==1){
    
    if(idx_exist$exist[i] == 0){
    
        print(paste(app_tbl$acc[i], app_tbl$tbl_nm_ap[i])) #asis의 테이블 명 확인
      stop('table do not exist')
    
      } 
    
    if(idx_exist$exist[i] > 1) stop('tbl name is duplicate')
  }
}

output <- cbind(app_tbl, idx_exist)

### 저장
write.csv(output,outputName,row.names=FALSE, fileEncoding = 'UTF-8')
print('f140  table existence test finished.')