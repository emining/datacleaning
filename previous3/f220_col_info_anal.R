# 컬럼 분석

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

## 파일 취합

## 파일 읽기 
if(sid == 'pmislgc'){
    input <- read.csv('output/a02_schemaTbl_ptold.csv',
        fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)
}else if(sid == 'pmissis'){
    input <- read.csv('output/a02_schemaTbl_pmissis.csv',
        fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)
}


cat('\n')
cat('\n')

## 분석 시스템
if(sid == 'pmislgc') print('PT_OLD column Analysis')
if(sid == 'pmissis') print('PMIS_SIS couumn Analysis')
cat('\n')

print('b0201::: Number of columns for analysis')
print(nrow(input))
cat('\n')
print('b0202::: Number of columns which has data')
print(dim(subset(input, count > 0))[1])


cat('\n')
cat('\n')

