### b04 Frequency Analysis for each column which is used in application 
### Not all data

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

dbName = "pmissis"

## 기존 해석 결과 지우기
sysName <- Sys.info()['sysname']
if(sysName == 'Linux'){

  if(dbName == "pmissis"){

    system('rm -rf ../output/o250_freq_pmissis_full/*')
    system('rm -rf ../output/o250_freq_pmissis_brief/*')

  }
}

## 컬럼정보 읽어오기
if(dbName == "pmislgc"){

    stop(' check the size at first')
    db_col <- read.csv('output/o120_schemaTbl_ptold.csv',
        fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)

}else if(dbName == "pmissis"){

    db_col <- read.csv('output/o120_schemaTbl_pmissis.csv',
        fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)

}

## APP 분석 정보 컬럼 정보 읽어오기
## 아래 파일은 한번 데이블 정의서를 한번 검증을 거친 것임
if(dbName == 18){

    app_col <- read.csv('input/',
        fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)

}else if(dbName == 20){

    app_col <- fReadPureUtf8Tbl('../output/o150_chk_col_pmissis.csv')
    
}
# remove unnecessary raw of which exist column value is not 1
app_col <- subset(app_col, exist == 1)

## Preprocess for Frequency Analysis 
## For make data.frame, the maximum row size must be known.

maxRowCount <- 0
nCol_app <- nrow(app_col)
nCol_db <- nrow(db_col)

###
for(i in 1:nCol_app){ #사용하는 컬럼에 대해서
  
  userNameApp <- app_col$ACC[i]
  tblNameApp <- app_col$Table_Id[i]
  colNameApp <- app_col$AS_IS_Column_Name[i]

  
  if(nchar(userNameApp) == 0) next 
  idxSelected <- subset(db_col,select = idx, userID == userNameApp & tblName == tblNameApp & 
                          COLUMN_NAME == colNameApp )
  idxSelected <- nrow(idxSelected)
  
  if(idxSelected == 1){ # if data exists
    if(dbName == "pmissis"){
        dataFileName <- idxSelected + 100000
    }else if(dbName == 20){
        dataFileName <- idxSelected + 200000  
    }else{
        stop('file do not exist')
    }
    dataFileNamePath = paste('output/a03_coldata/', dataFileName, '.csv',sep="")
    colData <- read.csv(dataFileNamePath, fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)
  }
  nrowNow <- nrow(colData)
  if(nrowNow > maxRowCount) maxRowCount <- nrowNow
}


### 기존에는 
eachFileColCount = 100
fileNum <- ceiling(nCol_app/eachFileColCount)
idxCol <- 0

tblName_df <-data.frame(matrix('',nrow = 1, ncol = 4),
                        stringsAsFactors=FALSE) 

colName_df <-data.frame(matrix('',nrow = 2, ncol = 4),
                        stringsAsFactors=FALSE) 


for(idxFile in 1:fileNum){
  outputTbl <- data.frame(matrix('',nrow = maxRowCount, ncol = eachFileColCount * 4),
                          stringsAsFactors=FALSE)  
  
  fDispLoof(idxFile,10,fileNum)
  
  for(idxColUnit in 1:eachFileColCount){
    ## initialize
    tblName_df[1,2] <- ""
    colName_df[1,2] <- ""
    colName_df[1,4] <- ""
    
    if(idxCol < nCol_app){
      idxCol <- idxCol +1
      
      userNameApp <- app_col$ACC[idxCol]
      tblNameApp <- app_col$Table_Id[idxCol]
      colNameApp <- app_col$AS_IS_Column_Name[idxCol]
      colNumberApp <- app_col$DB_colNumber[idxCol]
      tblNameLogic <- app_col$Table_Name[idxCol]
      colNameLogic <- app_col$AS_IS_Column_Comment[idxCol]
      
    
      if(nchar(userNameApp) == 0) next 
      
      
      idxSelectedRlt <- subset(db_col,select = c(idx,DATA_TYPE), userID == userNameApp 
                               & tblName == tblNameApp & COLUMN_NAME == colNameApp )
      idxVal <- idxSelectedRlt[1,1]
      idxSelected <- nrow(idxSelectedRlt)
      
      
      if(idxSelected == 1){ # if data exists
        tmpDataType <- idxSelectedRlt$DATA_TYPE[1]
        if(tmpDataType == 'BLOB') next
        if(tmpDataType == 'CLOB') next
      
        dataFileNamePath = paste('output/a03_coldata/', idxVal, '.csv',sep="")
        colData <- read.csv(dataFileNamePath, fileEncoding="UTF-8",stringsAsFactors= F, strip.white = TRUE)  
      }else{
        print(paste(userNameApp,tblNameApp,colNameApp,idxVal))
        print(idxCol)
        stop('col data do not exists')
      }
      
      
      freq_dec <- as.data.frame(sort(table(colData), decreasing = T), stringsAsFactors = FALSE)
      freq_inc <- as.data.frame(sort(table(colData), decreasing = F), stringsAsFactors = FALSE)
      
      
      
      # if(nrow(colFreq_dec)== 2 ) browser()
      ## 빈도 집합의 요소수가 2이상의 정상적인 경우에는 아래와 같이 나옴
      ##
      ##     var1   Freq
      ##  1  1      1144
      ##  2  2      698
      ##
      
      if(nrow(freq_dec) >= 2 ){
        freqTbl <- cbind(freq_inc, freq_dec)
        colFreqCount <- nrow(freqTbl)
        
      }else if(nrow(freq_dec) == 0){
        freqTbl <- data.frame(matrix('-', nrow = 1, ncol = 4))
        colFreqCount <- 0
        
      }else if(nrow(freq_dec) == 1){
        freqTbl <- data.frame(matrix(NA, nrow=1, ncol=4))
        tmpItem <-rownames(freq_dec)
        tmpCount <- freq_dec[1,1]
        
        freqTbl[1] <- tmpItem
        freqTbl[2] <- tmpCount
        freqTbl[3] <- tmpItem
        freqTbl[4] <- tmpCount
        colFreqCount <-1
      }else{
        stop('error')
      }
      
      colNames <- c( 'inc', 'freq','dec', 'freq')
      #names(freqTbl) <- colNames
      
      tblName_df[1,1] <- tblNameApp
      tblName_df[1,3] <- tblNameLogic
      colName_df[1,1] <- colNameApp
      colName_df[1,3] <- colNameLogic
      colName_df[2,] <- colNames
      colName_df[2,4] <- colNumberApp
      
      names(tblName_df) <-c('x1','x2','x3','x4')
      names(colName_df) <-c('x1','x2','x3','x4')
      names(freqTbl) <-c('x1','x2','x3','x4')
      
      combi_df <- rbind(tblName_df,colName_df,freqTbl)
      
   
      ## insert data to unit file
      startPoint <- (idxColUnit-1)*4 +1
      endPoint <- idxColUnit * 4
      names(outputTbl)[startPoint:endPoint] <- names(combi_df)
      
      ## 값 넣기
      col1 <- (idxColUnit-1)*4 +1 
      col2 <- idxColUnit * 4
      row1 <- 1
      row2 <- colFreqCount+3
      outputTbl[row1:row2, col1:col2] <- combi_df
    }
  }
  names(outputTbl)<-c(1:(eachFileColCount*4))
  
  outputTbl_brief <- outputTbl[1:100,]
  
  if(dbName == 18){
    print('not yet')
  }else if(dbName == 20){
    outputTblFileName.csv <- paste('output/b0401_freq_full/b04_freq_anal_pmissis',
                                   (idxFile+100), '.csv', sep = "")
    outputTblFileName_brief.csv <- paste('output/b0402_freq_brief/b0401_freq_anal_pmissis_brief',
                                         (idxFile+100), '.csv', sep = "")
    outputTblFileName_brief.xlsx <- paste('output/b0402_freq_brief/b0402_freq_anal_pmissis_brief',
                                         (idxFile+100), '.xlsx', sep = "")
    
  }
  
  write.csv(outputTbl, outputTblFileName.csv, row.names=FALSE)
  write.csv(outputTbl_brief, outputTblFileName_brief.csv, row.names=FALSE)
  write.xlsx(outputTbl_brief, outputTblFileName_brief.xlsx, row.names=FALSE, col.names = TRUE)
}

print('b04 finish')




