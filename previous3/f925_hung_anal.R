### 해운종합 컬럼명 형태소 분석
###
### input
###  1. 세종사전
###  2. 2차년도 단어사전
###  3. 3차년도 단어사전
###
### output
###  1. 형태소 분석으로 단어로 분할된 컬럼명의 분석 결과
###  2. 2,3 차년도 단어로 매칭 여부 
###
### Algorithm
### 1. 각 컬럼명의 논리명과 물리명을 읽어들인다.
###  1.1 논리명을 형태소 분할한다.
###  1.2 물리명을 언더바 기준으로 분할한다.
### 2. 분할이 정상적으로 이루어졌는지를 확인
###  2.1 논리명의 분할 개수와 물리명의 분할 개수가 맞는지를 확인
###  2.1.1 개수가 다른 경우에 오분할이 된다.
###  2.2 분할 개수가 동일한 경우에, 논리명과 물리명을 1:1로 매칭해서 단어리스트를 도출한다.
### 3.  2에서 도출된 리스트에 대해서 기존 사전과의 매칭여부를 확인한다.
###  3.1 매칭에 통과하는 것은 패스
###  3.2 매칭에 통과하지 않는 것은 print로 출력을 하게 하고, 이를 바탕으로 컬럼명을 수정을 함 
### 4. 위의 모든 과정을 통해서 컬럼명이 패스를 하면 종료 
###
###
### history
### history
### 

print('f412 해운종합 컬럼명 메타 표준화 ' )

rm(list=ls())
#options(error = recover)
library(KoNLP)

source("f002_common_fn.R", encoding='UTF-8')

inputFileName <- 'input/pmissis_col_180212.csv'
input_meta_word2_nm <- 'input/meta_word2.csv'
input_meta_term2_nm <- 'input/meta_term2.csv'

inputCol <- fReadPureUtf8Tbl(inputFileName)
inputCol <- subset(inputCol, select = c(col_nm_tl, col_nm_tp), subset=(col_use != 0))
metaword_2nd <- fReadPureUtf8Tbl(input_meta_word2_nm)
metaterm_2nd <- fReadPureUtf8Tbl(input_meta_term2_nm)


nC <- nrow(inputCol) # 컬럼개수
nword <- nrow(metaword_2nd)
nterm <- nrow(metaterm_2nd)


## 사용할 단어에 설정
#useSejongDic()

### 표준용어사전 머지

wordDic <- data.frame(matrix("ncn", nrow = nword, ncol = 2))
wordDic[,1] <- metaword_2nd$nm_log

#browser()

buildDictionary(ext_dic = "woorimalsam", user_dic=wordDic,replace_usr_dic = T)

#buildDictionary(ext_dic = "woorimalsam", user_dic=data.frame("ID", "ncn"),replace_usr_dic = T)

#mergeUserDic(wordDic)

#mergeUserDic(data.frame("코드","ncn"))


#for(i in 1:nword){
   
#mergeUserDic(wordDic)
   
#}
 
# --

## extract None from column name
for( i in 1:nC){
  
  sentence <- inputCol$col_nm_tl[i]
  #rlt <- extractNoun(sentence,autoSpacing = T)
  rlt <- extractNoun(sentence)
  print(rlt)

}


