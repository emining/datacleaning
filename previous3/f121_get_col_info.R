### 테이블 구조를 이용해서 컬럼의 세부정보를 불러오기 
### 
###
### input
### - o111_.. 테이블 구조
### 
### output
### - 
###

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')


### 전처리
dbname = "pmissis"
if(dbname == "pmislgc"){
    
    inputname <- "../output/o110_tbl_info_pmislgc.csv"
    outputName <- "../output/o120_schemaTbl_ptlgc.csv"

}else if(dbname == "pmissis"){

    inputname <- "../output/o110_tbl_info_pmissis.csv"
    outputName <- "../output/o120_schemaTbl_pmissis.csv"

}
input <- fReadPureUtf8Tbl(inputname)

connectInfo <- fSetIp(dbname)
id <- connectInfo[1]
pw <- connectInfo[2]
sid <- connectInfo[3]
ip <- connectInfo[4]
port <- connectInfo[5]

nTbl <- nrow(input)
for(i in 1:1){

    accName <- input$acc[i]
    accType <- input$acc_type[i]
    tblName <- input$tbl_nm_ap[i]
    
    ## 작업 시작
    print(paste('f121:::',(i+10000), 'user::', accName,'-table::',
          tblName,'start' ))
    ## 테이블의 컬럼 구조정보 가지고 오기
    conn <- fDBconnect(ip,port,sid,id,pw)
    querySentence <- paste('select COLUMN_NAME, DATA_TYPE, 
            DATA_LENGTH, NULLABLE from USER_TAB_COLUMNS 
            where TABLE_NAME=\'', tblName, '\' order by column_id', 
            sep="")
    getQuery <- fDBgetQuery(conn, querySentence)
        

}



dbDisconnect(conn)













fReadPureUtf8Tbl(outputName )


