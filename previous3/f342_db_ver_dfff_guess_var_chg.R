### DB 버전 비교 시트에서 대응이 안되는 부분은 아래와 같다.
### use_col 이 2인 컬럼은 새로 등록된 것인지, 아니면 기존에 있던 컬럼명이 변경이 된 것인지 알수가 없음
### 따라서 이전 비교 버전의 같은 테이블 명에서 컬럼명을 모아서 편집거리를 이용해서 후보군을 모집한다. 
###
###
### input
### - 비교 시트, 여기에는 use_col 란이 있어야 함.
### - 비교시트에서 번경전의 컬럼 명을 알고 싶은 시트(이번 과업에서는 2차DB)
###
### output
### - input 비교시트의 use_col의 값이 2 인 컬럼에 대해서 col_nm_tp1과 col_nm_tl1의 추축값
###
### 주의사항
### - 논리명을 추출하기 위해서는 각 테이블 별로 논리 물리명의 쌍으로 도출해야함.
### - 컬럼 리스트를 DF의 요소에 넣을 수 없기 때문에 리스트 데이터 형식을 이용해서 저장한다. 
###
### 데이터 형태 
### listTbl[[]] - 테이블명이 들어간 리스트
### listCol[[]] - 컬럼명이 들어간 리스트
### 
###
###
###
### Algorithm
### 1. 전처리
### 1.1 파일 로딩
### 1.2 이전 버전의 DB의 테이블 별 컬럼 리스트 작성
###  
### 2. 컬럼 명 매칭
### 2.1 col_use == 2 인 경우
### 2.1.2 기존시트테이블리스트에서 테이블명 매칭
### 2.1.2.1 테이블 명이 매칭이 된 경우
### 2.2 col_use 이 2가 아닌 경우에는 패스
###
###
###
###
###
###
###


### 1. 전처리
### 1.1 파일 로딩
rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

library(stringdist)


inputColNameOld <- 'input/db_def_1st.csv'
inputColNameDiff <- '../output/o341_db_diff_new.csv'

output_name <- '../output/o342_db_diff_new1.csv'

inputOld <- fReadPureUtf8Tbl(inputColNameOld) 
inputDiff <- fReadPureUtf8Tbl(inputColNameDiff)
output <- inputDiff
nOut <- nrow(output)

nC1 <- nrow(inputOld)
nC2 <- nrow(inputDiff)

### 1.2 이전 버전의 DB의 테이블 별 컬럼 리스트 작성
tblList <- as.list(unique(inputDiff$tbl_nm_tp1))
nTbl <- length(tblList)

colPigList <- list()
colLogList <- list()


errCo <- 0

for(i in 1:nTbl){
  
  tblName <- tblList[[i]]
  rlt <- subset(inputOld, select = c(col_nm_tl, col_nm_tp) , subset = (tbl_nm_tp == tblName))
  #print(nrow(rlt))                

  if(nrow(rlt) >= 1){

    colCo <- nrow(rlt)
    tmpPigList <- ""
    tmpLogList <- ""

    for(j in 1:colCo){

      #browser()
      tmpLogList <- c(tmpLogList,  rlt$col_nm_tl[j])
      tmpPigList <- c(tmpPigList,  rlt$col_nm_tp[j])

    }
  }

  colLogList[[i]] <- tmpLogList
  colPigList[[i]] <- tmpPigList

}



### 2. 컬럼 명 매칭
### 2.1 col_use == 2 인 경우

for(i in 1:nOut){
  
  tblNameTp <- output$tbl_nm_tp1[i]
  colNameTpNew <- output$col_nm_tp1[i]
  colNameTpPrv <- output$col_nm_tp2[i]

  col_use <- output$col_use[i]

  ### 2.1.2 기존시트테이블리스트에서 테이블명 매칭

  for(j in 1:nTbl){

    tblNameOld <- tblList[[j]]
        
    if(tblNameTp == tblNameOld){

      colLogCandiGrp <- colLogList[[j]]
      colPigCandiGrp <- colPigList[[j]]
      break

    }
  }

  rlt <- amatch(c(colNameTpNew), colPigCandiGrp, maxDist=5)
  #print(rlt)
  
  if(is.na(rlt)){
    
    #print(i)
    next() # 매칭된 답이 없는 경우
    
  }
  
  if(!is.na(rlt)){
    
    preColNameCandi <- colPigCandiGrp[[rlt]]
    
    if(colNameTpNew != preColNameCandi){
    
        print(c(colNameTpNew, preColNameCandi, colNameTpPrv))
        output$candi[i] <- preColNameCandi
      
    }
    
    if(nchar(colNameTpPrv) != 0){
      
      if(preColNameCandi != colNameTpPrv){
        
        errCo <- errCo + 1
        #print(c('err', errCo))
        
      }
    }
  }
}

write.csv(output, output_name, row.names = FALSE, fileEncoding="UTF-8", quote = FALSE)

