## 입력파일의 테이블과 컬럼 간 오류 확인
## 테이블에서는 사용안하는 걸로 되어 있는데 컬럼에서 사용한다고 되어 있으면
## 체크 컬럼을 확인해서 체크가 1이면 넘어가고 아니면 확인하도록 함

print('f191 check input db doc and output db doc')

### 전처리
rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

inputColName2 <- '../output/o190_tobe_col.csv'

app_col2 <- fReadPureUtf8Tbl(inputColName2)

nColApp <- nrow(app_col)
nCol2 <- nrow(app_col2)

for(i in 1:nColApp){
  idxMatch <- 0
  
  fDispLoof(i, 1000 , nColApp)
  tblName1 <- app_col$tbl_nm_tp[i]
  colName1 <- app_col$col_nm_tp[i]
  colUse1 <- app_col$col_use[i]
  colNo <- app_col$no[i]
  idxMatch <- 0
  
  if(colUse1 != 0){
    
    for(j in 1:nCol2){
      
      tblName2 <- app_col2$tbl_nm_tp[j]
      colName2 <- app_col2$col_nm_tp[j]
      
      if(tblName1 == tblName2){
        
        if(colName1 == colName2){
          
          #if(app_col$no[i] == 6959 & app_col2$no[j] == 6959) browser()
          
          idxMatch <- 1
          next()   
          
        }
      }
    } 
  }else{
    next()
  }
  if(idxMatch == 0){
    
    print(paste('no::', colNo , 'tbl::' , tblName1 , 'col::' , colName1, 'do not exists.' ))
    
  }
}

print('f191 has finished')

