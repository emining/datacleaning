### 2차년도 메타 데이터의 논리명 물리명 중복 확인
### 입력데이터
###  input/meta_word2.csv
###
### 출력 데이터
### 중복 메세지
###
### history
### 180207 - 작성 
###    중복 확인했는데 이상없음. 

print('f411 2차년도 메타 용어 중복 검토' )

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

inputMetaName <- 'input/meta_term2.csv'

meta_df <- fReadPureUtf8Tbl(inputMetaName)
nR <- nrow(meta_df)

## 메타명 중복 nm
## unique를 걸어서 개수가 줄어들면 중복이 있는 것이므로 체크를 해야함.

meta_nm <- as.data.frame(meta_df$nm)
meta_log <- as.data.frame(meta_df$metaNm_log)
meta_pig <- as.data.frame(meta_df$metaNm_pig)

idxDup <- 0

## metaNm  dup check
size1 <- nrow(meta_nm)
size2 <- nrow(unique(meta_nm))

if(size1 == size2){
  print('meta_nm is unique')
}else{
  idxDup <- 1
   print('meta_nm is not unique')
}

if(idxDup == 1){
  
  for(i in 1:(nR-1)){
    
    no1 <- meta_df$no[i]  
    name1 <- meta_nm[i,1]
    
    
    for(j in (i+1):nR){
      
      no2 <- meta_df$no[j]
      name2 <- meta_nm[j,1]
      
      if(name1 == name2){
        
        print(paste(no1, '::' , name1, '-', no2, '::' , name2, 'is same.'))  
        
      }
    }
    
  }
}


# meta_log(논리명) 중복 체크

size1 <- nrow(meta_log)
size2 <- nrow(unique(meta_log))

if(size1 == size2){
  print('meta_log is unique')
}else{
  idxDup <- 1
  print('meta_log is not unique')
}

if(idxDup == 1){
  
  for(i in 1:(nR-1)){
    
    no1 <- meta_df$no[i]  
    name1 <- meta_log[i,1]
    
    
    for(j in (i+1):nR){
      
      no2 <- meta_df$no[j]
      name2 <- meta_log[j,1]
      
      if(name1 == name2){
        
        print(paste(no1, '::' , name1, '-', no2, '::' , name2, 'is same.'))  
        
      }
    }
    
  }
}

# meta_pig(물리명) 중복 체크

size1 <- nrow(meta_pig)
size2 <- nrow(unique(meta_pig))

if(size1 == size2){
  print('meta_ig is unique')
}else{
  idxDup <- 1
  print('meta_pig is not unique')
}

if(idxDup == 1){
  
  for(i in 1:(nR-1)){
    
    no1 <- meta_df$no[i]  
    name1 <- meta_pig[i,1]
    
    
    for(j in (i+1):nR){
      
      no2 <- meta_df$no[j]
      name2 <- meta_pig[j,1]
      
      if(name1 == name2){
        
        print(paste(no1, '::' , name1, '-', no2, '::' , name2, 'is same.'))  
        
      }
    }
    
  }
}

