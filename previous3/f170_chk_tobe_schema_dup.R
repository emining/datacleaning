### To-Be 모델의 테이블 및 컬럼명 중복을 확인
### 입력 - 테이블 정의서의 테이블 및 컬럼 시트(각각 파일) 
### 출력 - 중복 테이블 및 컬럼 리스트
### 주요기능
### 1. 테이블 리스트에서 사용되는 테이블(1or2)의 테이블 명 중복 체크
###
### TODO
###
### 작성이력
### 171227 - 작성시작 
### 180115
###    - 컬럼 리스트의 tobe테이블명과 테이블 리스트의 테이블 의 논리명에 차이가 있을 수 있으므로
###    - 테이블 리스트의 논리명으로 TO_BE_TABLE_LOGIC를 생성해서 추가. 

print('f170_chk_tobe_schema_dup')

### 전처리
rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

## input data is what is checked by a05 and a06 process
inputTblName <- '../output/o140_chk_tbl_pmissis.csv'
inputColName <- '../output/o150_chk_col_pmissis.csv'

## step01 전처리 
## print('step01 전처리)
inputTbl <- fReadPureUtf8Tbl(inputTblName) 
inputCol <- fReadPureUtf8Tbl(inputColName)

## step02 테이블 논리/물리명 중복 체크
print('step02 테이블 중복체크')
## 입력테이블의 tobe 테이블 명의 중복 확인

nTbl <- nrow(inputTbl)

## 사용하는 테이블(table_use가 1 or 2)에 대해서 테이블 명의 중복 체크
useTblName_df <- subset(inputTbl,  exist == 1 | exist == 2)
#useTblName_df <- subset(useTblName_df, type == 'TABLE')

nTbl <- nrow(useTblName_df)
dup_df <- data.frame(matrix(0,nrow = nrow(useTblName_df), ncol = 1))

## 테이블 물리/논리명 중복확인 
for(i in 1:(nTbl-1)){
  name1_p <- useTblName_df$tbl_nm_tp[i]
  name1_l <- useTblName_df$tbl_nm_tl[i]
    for(j in (i+1):nTbl){
        if(dup_df[i,1] == 0 ){
          name2_p <- useTblName_df$tbl_nm_tp[j]
          name2_l <- useTblName_df$tbl_nm_tl[j]
          if(name1_p == name2_p){
            dup_df[i,1] = 1
            dup_df[j,1] = 1
            print(paste(i, useTblName_df$tbl_nm_tp[i]))
            print(paste(j, useTblName_df$tbl_nm_tp[j]))
          }
          if(name1_l == name2_l){
            dup_df[i,1] = 1
            dup_df[j,1] = 1
            print(paste(i, useTblName_df$tbl_nm_tl[i]))
            print(paste(j, useTblName_df$tbl_nm_tl[j]))
          }
        }
    }
}


## step03 To-Be 컬럼명 중복 체크
print('step03 컬럼명 중복 체크')
## At first, the column which is used to be model is extracted, but the table of that column may be not used.
## So the usage of table must be checked firstly
useColName_df <- subset(inputCol, col_use == 1 | col_use == 2 )
useColName_df <- subset(useColName_df, type == 'TABLE')
nCol <- nrow(useColName_df)
dupCol_df <- data.frame(matrix(0, nrow = nCol, ncol = 1))

for(i in 1:(nCol-1)){
  tblName1 <- useColName_df$tbl_nm_tp[i]
  colName1_p <- useColName_df$col_nm_tp[i]
  colName1_l <- useColName_df$col_nm_tl[i]
  
  existCol1 <- useColName_df$exist[i]
  
  ## 테이블이 사용하는 컬럼에 대해서만 중복을 확인함.
  if(nrow(subset(useTblName_df, tbl_nm_tp == tblName1)) == 1){ # for column of which is used to To-Be model
    if(existCol1 == 1 | existCol1 == 2)
      for(j in (i+1):nCol){
        if(dupCol_df[i,1] == 0){
          tblName2 <- useColName_df$tbl_nm_tp[j]
          colName2_p <- useColName_df$col_nm_tp[j]
          colName2_l <- useColName_df$col_nm_tl[j]
          
          existCol2 <- useColName_df$exist[j]
          if(existCol2 == 1 | existCol2 == 2)
            if(tblName1 == tblName2){
              if(colName1_p == colName2_p){
                dupCol_df[i,1] = 1
                dupCol_df[j,1] = 1
                print(paste(useColName_df$no[i] , useColName_df$tbl_nm_tp[i], useColName_df$col_nm_tp[i]))
                print(paste(useColName_df$no[j] , useColName_df$tbl_nm_tp[j], useColName_df$col_nm_tp[j]))
              }
              if(colName1_l == colName2_l){
                dupCol_df[i,1] = 1
                dupCol_df[j,1] = 1
                print(paste(useColName_df$no[i],useColName_df$tbl_nm_tp[i], useColName_df$col_nm_tl[i]))
                print(paste(useColName_df$no[j],useColName_df$tbl_nm_tl[j], useColName_df$col_nm_tl[j]))
              }
            }
        } 
      }
  }
}

print('f170 has finished')
