################################################################################
## 업체 코드 분석
## 2,3차 사업자 등록번호를 기준으로 동일 업체 구분
## 
## 분석 내용
## 1. 2, 3차 업체코드 중복여부 확인



rm(list=ls())
source("libcommon.R", encoding='UTF-8')


##############################
## db connect
##



dbName <- "pmislgc"
conn <- fDBconnect2(dbName)
## 2차 업체코드
qs <- "select * from DPBZO.PTB_COMM1012"
cmpy2 <- dbGetQuery(conn, qs)

## 2차 사용자
qs <- "select * from DPBZO.PTB_COMM1010"
usr2 <- dbGetQuery(conn, qs)

dbDisconnect(conn)

dbName <- "pmissis"
conn <- fDBconnect2(dbName)

## 3차 업체코드
qs <- "select * from MBLS.MBBS_COMPANY"
cmpy3 <- dbGetQuery(conn, qs)

## 사용자정보(민원인)
qs <- "select * from MCSS.MCSS_USERINFO"
usr3 <- dbGetQuery(conn, qs)

dbDisconnect(conn)

stop()
usr2 <- usr2 %>%
     select(USER_ID, BIZRNO)

str(usr2)

cmpy2a <- cmpy2 %>%
    select(HARBOR_ENTRPS_CD, # 항만업체코드
           BIZRNO # 사업자등록번호
           ) %>%
    arrange(BIZRNO)

usr3 <- usr3 %>%
    select(USER_ID, BUSINESS_NO)

cmpy3a <- cmpy3 %>%
    select(COMPANY_NO, # 업체코드
           UPJONG_CODE, # 업종코드
           MIG_REG_NO, # 면허정보
           BIZ_NO #사업자 등록번호
           ) %>%
    arrange(BIZ_NO)

names(usr2) <- c("userid", "bizno")
names(cmpy2a) <- c("cmpyno", "bizno")
names(usr3) <- c("userid", "bizno")
names(cmpy3a) <- c("cmpyno_seq", "upjongcode", "cmpyno", "bizno")

usr2a <- left_join(usr2, cmpy2a)
usr3a <- left_join(usr3, cmpy3a)

unique(usr2a$cmpyno)
unique(usr3a$cmpyno)

stop()

## length(intersect(usr2a$cmpyno, usr3a$cmpyno)) 
## [1] 1223
## 업체정보가 1223개 중복됨



##############################
## 1. 2, 3차 업체코드 중복여부 확인
##

## dim(usr2a)
## [1] 25680     3
## length(unique(usr2a$cmpyno))
## [1] 7544
## 25680 - 7544 = 18136 개가 중복데이터

## dim(usr3a)
## [1] 1570976       5
## length(unique(usr3a$cmpyno))
## [1] 4834
## 1570976 - 127 = 86 개가 중복 데어터임

## 2,3차 사업자 등록번호가 중복인 것 찾기

## cmpy2b <- cmpy2a %>%
##     filter(cmpy2a$bizno %in% cmpy3a$bizno)

## cmpy3b <- cmpy3a %>%
##      filter(cmpy3a$bizno %in% cmpy2a$bizno)
table(cmpy2a$bizno)
table(cmpy3a$bizno)
stop()


nr2b <- nrow(cmpy2b)
nr3b <- nrow(cmpy3b)

intersect(cmpy2a$bizno, cmpy3a$bizno) 




## 결손치처리
for(i in 1:nr2b){
    
    if(is.null(cmpy2b$bizno[i])) cmpy2b$bizno[i] <- "_NULL_"
    if(is.na(cmpy2b$bizno[i])) cmpy2b$bizno[i] <- "_NULL_"
}

for(j in 1:nr3b){
    
    if(is.null(cmpy3b$bizno[j]))  cmpy3b$bizno[j] <- "_NULL_"
    if(is.na(cmpy3b$bizno[j]))  cmpy3b$bizno[j] <- "_NULL_"
}
    

match_df <- fMakePureDF(0, nr2b,1)

for(i in 1:nr2b){
    
    for(j in 1:nr3b){

        if(cmpy2b$bizno[i] == cmpy3b$bizno[j]){

            match_df[i,1] <- 1

        }
    }
}
    
same23_df <- cmpy2b[match_df == 1,]
