## 2,3차 사용자 중복 체크
## 
## 중복체크 기준
## 1. 2차, 3차 각각에 대해서 아이디 중복 체크
## 1-1. 3차의 경우에는 

## 조건 : source로 실행해야 함(윈도우에서만) 
## source("anal_useraccount3.R", encoding = "UTF-8")

rm(list=ls())
source("libcommon.R", encoding='UTF-8')

##############################
## db connect
##
dbName <- "pmissis"
conn <- fDBconnect2(dbName)

## 민원인
qs2 <- "select * from MCSS.MCSS_USERINFO"
busr <- dbGetQuery(conn, qs2)

## 내부사용자
qs3 <- "select * from MCSS.ORG_USER"
cusr <- dbGetQuery(conn, qs3)

dbDisconnect(conn)

dbName <- "pmislgc"
conn <- fDBconnect2(dbName)
qs5 <- "select * from DPBZO.PTB_COMM1010"
eusr <- dbGetQuery(conn, qs5)

dbDisconnect(conn)


## dim(busr)
## [1] 797  42

## str(busr)
## 'data.frame':	797 obs. of  42 variables:
##  $ USER_ID       : chr  "hnrship" "imtinc" "sinokor01" "jangum1" ...
##  $ USER_NAME     : chr  "김재윤" "정의용" "정태순" "김남덕" ...
##  $ PASSWD_REQ    : chr  "전화번호" "질문" "나는" "우리에게 필요한건?" ...
##  $ PASSWD_RES    : chr  "전화번호" "회사" "너" "휴가" ...
##  $ PASSWD        : chr  "UpPvEm6QZoCBOOmjDFjkZhc2VkMvjz1fctf9t8iX7OY=" "s+opD6X405OBf37wgYlJ5lKENNPH2hAz5pY/LT1DNEo=" "iH4a+UCRFhCohubudHz5tnvuz12d97dbputtHbEFZbo=" "ccuRj+K1OyF43O0unDLaJdkClQh4BrnEbuqyUYDt7+k=" ...
##  $ RASSEDIT_DATE : chr  "" "14/12/04" "" "13/01/17" ...
##  $ FOREIGN_GB    : int  1 1 1 1 1 1 1 1 1 1 ...
##  $ USER_GB       : int  3 3 3 3 3 3 3 3 3 3 ...
##  $ COMPANY_NAME  : chr  "하나로해운(주)" "주식회사 아이엠티 인코퍼레이션" "장금상선주식회사" "장금마리타임(주)" ...
##  $ SSN           : chr  "H6o8r8kr7dLz767a1+PNIQ==" "2LeUV+2I87KwI1ADfQ4kqA==" "q/xazsDfjvft5JJgBJoRQA==" "5AGCqRMmbMq9kz8exZ6+eg==" ...
##  $ CORPORATE_NO  : num  1.10e+12 1.18e+12 1.10e+12 1.10e+12 1.10e+12 ...
##  $ BUSINESS_NO   : num  1.02e+09 6.03e+09 1.05e+09 1.05e+09 1.02e+09 ...
##  $ ADDR_GB       : int  2 2 2 1 2 2 2 2 2 2 ...
##  $ POST_NO       : int  3188 48060 4526 4526 4512 6225 7345 3173 37727 4533 ...
##  $ ADDR_BASIC    : chr  "서울특별시 종로구 청계천로 35 (서린동)" "부산광역시 해운대구 APEC로 58 (우동)" "서울특별시 중구 세종대로 58 (남대문로4가)" "서울특별시 중구 세종대로 64 (태평로2가)" ...
##  $ ADDR_DETAIL   : chr  "88번지 서린빌딩 1202호" "3302호" "동성빌딩 3충" "해남빌딩 923호" ...
##  $ EMAIL_ID      : chr  "goldbs" "mail" "unimark" "realsmy" ...
##  $ EMAIL_URL     : chr  "hanaroshipping.com" "imtline.com" "sinokor.co.kr" "sinokor.co.kr" ...
##  $ TEL_NO_DDD    : int  2 51 2 2 2 2 2 2 54 51 ...
##  $ TEL_NO_KUK    : int  6712 745 6496 779 2003 2110 780 739 275 440 ...
##  $ TEL_NO_NO     : int  6006 9302 7224 2279 1144 6368 7355 1551 3531 5512 ...
##  $ PHONE_NO_DDD  : int  10 10 10 10 NA NA NA NA 10 10 ...
##  $ PHONE_NO_KUK  : int  7278 8688 5289 7517 NA NA NA NA 6787 6657 ...
##  $ PHONE_NO_NO   : int  3554 5151 499 9353 NA NA NA NA 4573 477 ...
##  $ FAX_NO_DDD    : int  2 51 2 2 2 NA 2 2 54 51 ...
##  $ FAX_NO_KUK    : int  735 745 774 779 2003 NA 780 737 278 466 ...
##  $ FAX_NO_NO     : int  5420 9400 1423 2477 1002 NA 7356 8628 2161 7825 ...
##  $ REG_DATE      : chr  "12/02/03" "12/02/03" "12/02/03" "12/02/07" ...
##  $ CAKEY         : chr  "cn=하나로해운(주)_0000171339,ou=KTNET,ou=AccreditedCA,o=TradeSign,c=KR" "" "" "" ...
##  $ MEM_APPOVAL_YN: chr  "Y" "Y" "Y" "Y" ...
##  $ GRADE_CD      : int  1 1 1 1 1 1 1 2 1 1 ...
##  $ CREATE_DATE   : chr  "12/02/03" "12/02/03" "12/02/03" "12/02/07" ...
##  $ CREATE_USERID : chr  "hnrship" "imtinc" "sinokor01" "jangum1" ...
##  $ UPDATE_DATE   : chr  "" "14/12/04" "" "13/01/17" ...
##  $ UPDATE_USERID : chr  "" "imtinc" "" "jangum1" ...
##  $ COMPANY_NO    : int  242 4013 172 214 8102609 NA NA NA NA 9 ...
##  $ LOGIN_FAIL_CO : int  NA NA NA NA NA 1 NA 5 NA 0 ...
##  $ USER_IP       : logi  NA NA NA NA NA NA ...
##  $ PASSPORT_CHUNG: chr  "" "" "" "" ...
##  $ PASSPORT_YY   : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ PASSPORT_CHECK: int  NA NA NA NA NA NA NA NA NA NA ...
##  $ PASSPORT_SEQNO: int  NA NA NA NA NA NA NA NA NA NA ...


## dim(cusr)
## [1] 4761   17

## str(cusr)
## 'data.frame':	4761 obs. of  17 variables:
##  $ EX_USER_ID   : chr  "in13043917" "in13043933" "in13040246" "in13043938" ...
##  $ NAME         : chr  "권현욱" "윤순구" "전병화" "박유신" ...
##  $ PWD          : chr  "a6d58ee0683fd78a693e49df14097e770301b72a" "4c3bf8e0ed2a42c8cab226b7f802789205275e72" "fe9c0d6d5dd9ef57ef069ed5e636816de43baa46" "34a449127c4e24eccc8b15155d769c2dd8ec0a80" ...
##  $ EMAIL        : chr  "hwkwon@korea.kr" "yoontree@korea.kr" "j1179jk@naver.com" "soosoman@korea.kr" ...
##  $ PHONE        : chr  "051 400 5610" "033 520 6161" "054-4672" "061 660 9028" ...
##  $ EX_JK_CODE   : int  91 92 5458 23117 22990 93 17353 23119 23122 23120 ...
##  $ EX_JW_CODE   : chr  "0013600" "0013600" "A05458" "B00001" ...
##  $ EX_OU_CODE   : int  1192074 1192407 1192438 1192470 1192505 1192379 1192518 1192536 1192128 1192079 ...
##  $ EX_FAX       : chr  "051    400 5745" "033    520 6160" "" "061    682 8214" ...
##  $ EX_ACTIVE    : chr  "1" "1" "1" "1" ...
##  $ EX_DN        : chr  "cn=138권현욱001,ou=people,ou=해양수산부,o=Government of Korea,c=KR" "cn=138윤순구001,ou=people,ou=해양수산부,o=Government of Korea,c=KR" "" "cn=138박유신001,ou=people,ou=해양수산부,o=Government of Korea,c=KR" ...
##  $ EX_O_ZIPCODE : int  49111 25752 37716 59713 59780 22346 57158 46079 46079 22382 ...
##  $ EX_O_ADDR_1  : chr  "부산광역시 영도구 해양로 337" "강원도 동해시  평원로 46" "경상북도 포항시 북구  해동로 376" "전라남도 여수시 여서1로 107" ...
##  $ EX_O_ADDR_2  : chr  "국립수산물품질관리원 운영지원과" "동해지방해양수산청 항만물류과" "" "전남 여수시 여서1로 107" ...
##  $ PER_ID       : chr  "" "" "" "" ...
##  $ LOGIN_FAIL_CO: logi  NA NA NA NA NA NA ...
##  $ USER_IP      : logi  NA NA NA NA NA NA ...


## dim(eusr)
## [1] 24376    70

## str(eusr)
## 'data.frame':	24376 obs. of  70 variables:
##  $ USER_MANAGE_NO           : int  21911 21912 21913 21914 21915 21916 21917 21918 21919 21920 ...
##  $ BIZRNO                   : chr  "" "" "" "" ...
##  $ USER_ID                  : chr  "ykebs" "ykebs645" "ykh690" "ykh900" ...
##  $ USER_PASSWD              : chr  "FWtqBN9FmhdGujDWzuwwOa8hENyifYFuwBaMR9GKWKQ=" "FWtqBN9FmhdGujDWzuwwOa8hENyifYFuwBaMR9GKWKQ=" "FWtqBN9FmhdGujDWzuwwOa8hENyifYFuwBaMR9GKWKQ=" "FWtqBN9FmhdGujDWzuwwOa8hENyifYFuwBaMR9GKWKQ=" ...
##  $ PASSWD_HINT_CD           : chr  "" "WH" "WH" "WH" ...
##  $ PASSWD_CNSR              : chr  "" "SPIDC" "SPIDC" "SPIDC" ...
##  $ PASSWD_ERR_CO            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ NATIVE_FRGNR_SE          : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ USER_NM                  : chr  "장윤종" "장윤종" "육근형" "윤경환" ...
##  $ USER_SXDST               : chr  "M" "M" "M" "M" ...
##  $ USER_BRTHDY              : int  19810114 NA NA NA NA NA NA 19770609 NA NA ...
##  $ USER_TELNO               : chr  "" "" "" "032-570-6413" ...
##  $ USER_MOBILE_NO           : chr  "" "" "" "" ... 
##  $ USER_FXNUM               : chr  "" "" "" "032-570-6227" ...
##  $ USER_EMAIL_ADDR          : chr  "ykebs@naver.com" "ykebs@nate.com" "ykh690@kmi.re.kr" "" ...
##  $ EMAIL_RECPTN_AT          : chr  "Y" "Y" "Y" "Y" ...
##  $ USER_ZIP                 : chr  "" "07547" "" "22829" ...
##  $ USER_ADDR                : chr  "" "서울특별시 강서구 양천로 583, 우림블루나인 24층 " "" "인천광역시 서구 백범로677번길 9 " ...
##  $ USER_DETAIL_ADDR         : chr  "" "(염창동)" "" "(가좌동)" ...
##  $ USER_ENG_ADDR            : chr  "" "" "" "" ...
##  $ TRADE_ENTRPS_SE          : int  1 2 2 2 1 2 1 5 2 2 ...
##  $ INTRST_MATTER_CD         : int  1 9 9 9 2 5 9 4 9 9 ...
##  $ PSITN_INSTT_CD           : int  NA NA NA NA 1 NA 1 NA NA NA ...
##  $ CMPTNC_INSTT_CD          : chr  "" "" "" "" ...
##  $ PSITN_PRT_CD             : chr  "" "" "" "" ...
##  $ PSITN_DEPT_NM            : chr  "" "" "" "수출입팀" ...
##  $ QLMNGMT_INSTT_CD         : logi  NA NA NA NA NA NA ...
##  $ AUTHOR_GROUP_ID          : chr  "LOGIN" "LOGIN" "LOGIN" "LOGIN" ...
##  $ USER_SE                  : chr  "M" "M" "M" "M" ...
##  $ ID_LOGIN_PERM_AT         : chr  "Y" "Y" "Y" "Y" ...
##  $ ID_LOGIN_PERM_BEGIN_DE   : int  20171021 20171021 20171021 20171021 20171021 20171021 20171021 20171021 20171021 20171021 ...
##  $ ID_LOGIN_PERM_END_DE     : int  20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 ...
##  $ USE_REQST_CD             : int  3 3 3 3 9 3 3 3 3 3 ...
##  $ ENTRPS_USE_REQST_STTUS_CD: int  9 2 2 2 2 2 2 9 2 2 ...
##  $ ENTRPS_USE_REQST_RESN    : chr  "" "" "" "" ...
##  $ SELF_CRTFC_SE_CD         : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ PKI_VALUE                : chr  "" "" "" "" ...
##  $ IPIN_CRTFC_CD            : chr  "" "" "" "" ...
##  $ IPIN_PIN_CD              : chr  "" "" "" "" ...
##  $ MOBILE_REQST_DE          : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ MOBILE_REQST_AG_CD       : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ AUTHOR_CHANGE_REQST_RESN : chr  "" "" "" "" ...
##  $ PASSWD_END_DE            : int  20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 20180121 ...
##  $ CRTFC_DN_VALUE           : chr  "" "" "" "" ...
##  $ EMPL_NO                  : chr  "" "" "" "" ...
##  $ ESNTL_ID                 : chr  "" "" "" "" ...
##  $ FRST_SATMNT_DE           : int  NA 20091215 20110131 20090108 20120312 20090403 20030331 NA 20090512 20090614 ...
##  $ GCTS_ENTRPS_CD           : chr  "" "" "" "" ...
##  $ GRAD_CD                  : chr  "" "" "" "" ...
##  $ MLG_SM                   : int  NA NA NA NA NA NA NA NA NA NA ...
##  $ OFCPS_NM                 : chr  "" "" "" "" ...
##  $ ORGNZT_ID                : chr  "" "" "" "" ...
##  $ TMPR_NO_AT               : chr  "Y" "Y" "Y" "Y" ...
##  $ ERP_ID                   : chr  "" "" "" "" ...
##  $ PROJECT_CD               : chr  "" "" "" "" ...
##  $ PROJECT_CD2              : chr  "" "" "" "" ...
##  $ USER_ERR_STTUS_INFO      : chr  "" "" "" "" ...
##  $ USER_REQST_NO            : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ UPDUSR_ID                : chr  "ykebs" "3" "3" "3" ...
##  $ UPDT_DT                  : chr  "13/07/28" "09/12/15" "11/01/31" "09/01/08" ...
##  $ RGSTR_ID                 : chr  "MIG" "MIG" "MIG" "MIG" ...
##  $ REGIST_DT                : chr  "17/10/21" "09/12/15" "11/01/31" "09/01/08" ...
##  $ ROW_STTUS_CD             : chr  "A" "A" "A" "A" ...
##  $ ERP_USER_ID              : chr  "" "" "" "" ...
##  $ LAST_LOGIN_IP            : chr  "" "" "" "" ...
##  $ LAST_LOGIN_DT            : chr  "" "" "" "" ...
##  $ PSITN_DEPT_CD            : chr  "" "" "" "" ...
##  $ SPPRN_CD                 : chr  "" "" "" "" ...
##  $ ASIS_HARBOR_ENTRPS_CD    : logi  NA NA NA NA NA NA ...
##  $ SOURCE_SYSTEM            : logi  NA NA NA NA NA NA ...

##############################
## 사용자 비교를 위한 공통 변수 추출
## 컬럼명은 2차 테이블을 기준을 하고 아래와 같이 통일 
## USER_ID -- id
## USER_NM --name 
## USER_MOBILE_NO - telno 
## USER_EMAIL_ADDR  - email

##############################
## 민원인 처리
##
busr <- select(busr, USER_ID, USER_NAME, EMAIL_ID, EMAIL_URL, PHONE_NO_DDD, PHONE_NO_KUK, PHONE_NO_NO)

names(busr) <- c("id", "name", "email1","email2", "telno1",  "telno2",  "telno3")

##공백제거
busr$id <- str_replace_all(busr$id, "\\s", "") # 공백제거
busr$name <- str_replace_all(busr$name, "\\s", "") # 공백제거
busr$email1 <- str_replace_all(busr$email1, "\\s", "") # 공백제거
busr$email2 <- str_replace_all(busr$email2, "\\s", "") # 공백제거
busr$telno1 <- str_replace_all(busr$telno1, "\\s", "") # 공백제거
busr$telno2 <- str_replace_all(busr$telno2, "\\s", "") # 공백제거
busr$telno3 <- str_replace_all(busr$telno3, "\\s", "") # 공백제거

busr <- busr %>%
     mutate(email = str_c(email1, "@", email2, sep = "")) %>%
     mutate(telno = str_c(telno1, telno2, telno3, sep = "")) #%>%
     #select( -c(telno1, telno2, telno3, telno4, email1, email2))


##############################
## 내부사용자
##

## EX_USER_ID - id
## NAME       - name
## EMAIL      - email
## PHONE      - telno
cusr <- select(cusr, EX_USER_ID, NAME, EMAIL, PHONE)  
names(cusr) <- c("id", "name", "email","telno")

##공백제거
cusr$id <- str_replace_all(cusr$id, "\\s", "") # 공백제거
cusr$name <- str_replace_all(cusr$name, "\\s", "") # 공백제거
cusr$email <- str_replace_all(cusr$email, "\\s", "") # 공백제거
cusr$telno <- str_replace_all(cusr$telno, "\\s", "") # 공백제거


##############################
## 2차사용자 
##

eusr <- select(eusr, USER_ID, USER_NM, USER_TELNO, USER_EMAIL_ADDR )
names(eusr) <- c("id", "name", "telno","email")

##공백제거 
eusr$id <- str_replace_all(eusr$id, "\\s", "") # 공백제거
eusr$name <- str_replace_all(eusr$name, "\\s", "") # 공백제거
eusr$email <- str_replace_all(eusr$email, "\\s", "") # 공백제거
eusr$telno <- str_replace_all(eusr$telno, "\\s", "") # 공백제거


##############################
## 이름정제

##(주) 제거 - 윈도우에서는 안 없어진다. 
eusr$name <- str_replace_all(eusr$name, "\\(주\\)", "")
eusr$name <- str_replace_all(eusr$name, "\\(유\\)", "")

## table(eusr$name)
## table(busr$name)
## table(cusr$name)
## table(dusr$name)
## table(eusr$name)


##############################
## 전화번호 정제 

## 숫자이외에 다 제거

busr$telno <- str_replace_all(busr$telno, "[^0-9]", "")
cusr$telno <- str_replace_all(cusr$telno, "[^0-9]", "")

eusr$telno <- str_replace_all(eusr$telno, "[^0-9]", "")

## 전화번호 앞에 0붙이기

busr$telno <- str_replace(busr$telno,"^[1-6][0-9]+$", str_c("0", busr$telno, sep = ""))
cusr$telno <- str_replace(cusr$telno,"^[1-6][0-9]+$", str_c("0", cusr$telno, sep = ""))

eusr$telno <- str_replace(eusr$telno,"^[1-6][0-9]+$", str_c("0", eusr$telno, sep = ""))

##전화번호에서 숫자9자리미만은 결손치로 처리

busr$telno <- str_replace_all(busr$telno, "^[0-9]{1,7}$", NA_character_)
cusr$telno <- str_replace_all(cusr$telno, "^[0-9]{1,7}$", NA_character_)

eusr$telno <- str_replace_all(eusr$telno, "^[0-9]{1,7}$", NA_character_)

## 전화번호가 전부 0인것은 제외

busr$telno <- str_replace_all(busr$telno, "^[0]+$", NA_character_)
cusr$telno <- str_replace_all(cusr$telno, "^[0]+$", NA_character_)

eusr$telno <- str_replace_all(eusr$telno, "^[0]+$", NA_character_)


##############################
## 이메일 정제

## 전처리

## 형식이 안맞는 우편번호 NA 처리
## @가 포함이 안된 메일은 삭제
busr$email <- str_replace_all(busr$email, "^@", NA_character_)
busr$email <- str_replace_all(busr$email, "@$", NA_character_)
cusr$email <- str_replace_all(cusr$email, "^@", NA_character_)
cusr$email <- str_replace_all(cusr$email, "@$", NA_character_)


eusr$email <- str_replace_all(eusr$email, "^@", NA_character_)
eusr$email <- str_replace_all(eusr$email, "@$", NA_character_)

## 2, 3차로 통합
## 3차는 소스를 추가함


busr <- busr %>%
    mutate(src = "민원인")

cusr <- cusr %>%
    mutate(src = "내부사용자")

               
usr3 <- bind_rows(busr,cusr)
usr2 <- eusr 
rm(busr, cusr, eusr)


## ID, 중복

## 2,3차 각각에 대해서 아이디 중복 확인
## dim(usr2)
## length(unique(usr2$id))
## dim(usr2)
## 24376     4
## length(unique(usr2$id))
## [1] 24375

## 한명이 중복됨
## 2차에서 중복되는 아이디 찾기

usr2dup <- usr2[duplicated(usr2$id, fromlast = TRUE),]
usr2dup <- arrange(usr2dup, id, name)
## dim(usr2dup)
## [1] 1 4

## usr2dup
## 1 anchorking 서승휘  <NA>  <NA>
## 2 anchorking 서승휘  <NA>  <NA>


## id   name telno email
## 1 anchorking 서승휘  <NA>  <NA>
## 2 anchorking 서승휘  <NA>  <NA>
## 일단 2차 사용자에서 중복을 제거해야 함.

## length(unique(usr3$id))
## [1] 5557
## dim(usr3)
## [[1] 5558   10
## 3차는 일부 중복이 됨. 기존에 복수의 테이블을 취합해서 일지도 모름

## 3차에서 중복되는 id 정보를 추출
usr3dup <- fGetDupRow(usr3,1)

usr3dup <- usr3dup %>%
    arrange(id, name, telno, email) %>%
    select(id, name, telno, email, src)

fWritePureUtf8Tbl(usr3dup, "../output/usr3dup.csv")

## dim(usr3dup)
## [1] 2 5

## usr3dup
## in14070006 최가연 0442006214 gy_choi@korea.kr 내부사용자
## 2 in14070006 최가연       <NA>             <NA>     민원인


## 검토결과 같은 아이디인데 이름이 다른 경우가 있음. 
#3 3차 DB에서 정리가 필요

## 3차에서 2차 사용자가 있는지 체크(중복허용)
usr32dup <- usr3[usr3$id %in% usr2$id,]
usr32dup <- usr32dup %>%
    select(id, name, telno, email ,src) %>%
    arrange(id, name, telno, email)

fWritePureUtf8Tbl(usr32dup, "../output/usr32dup.csv")

## dim(usr32dup)
## [1] 128   5

## head(usr32dup)
##             id         name       telno                  email    src
## 1       bkship       최재원 01046761484          bk@bkship.com 민원인
## 2       bndjsc       손상완 01039298371     swson@djship.co.kr 민원인
## 3     bong3521 주식회시우국 01026082626 wookuk6219@hanmail.net 민원인
## 4       bosung     황보대정 01020368440  bosungceo@hanmail.net 민원인
## 5 centurybusan       김윤수 01090078231 centurybusan@naver.com 민원인
## 6     chokwang       서영춘 01090461922    chokwang@ckship.com 민원인


## 2차에 3차ID 사용자 역체크(중복허용)
usr23dup <-  usr2[usr2$id %in% usr3$id,]
usr23dup <- usr23dup %>%
    select(id, name, telno, email) %>%
    arrange(id, name, telno, email)

fWritePureUtf8Tbl(usr23dup, "../output/usr23dup.csv")


## dim(usr23dup)
## [1] 128   4

## head(usr23dup)
##           id     name      telno                   email
## 1       bkship   정치형 0518176073      bkship@hanmail.net
## 2       bndjsc 동진상선       <NA>      swson@djship.co.kr
## 3     bong3521   정성희 0616426220  wookuk6219@hanmail.net
## 4       bosung 보성해운       <NA>                    <NA>
## 5 centurybusan   김윤수       <NA>  centurybusan@naver.com
## 6     chokwang 조광해운       <NA> chokwang@ckshipping.com

## 이름과 아이디가 동일한 케이스를 찾음
## 결과물로는 동일 아이디에 대해서 각각의 이메일과 전화번호를 발췌한다.
usr2a <- usr2 %>%
    select(id, name, telno,  email) %>%
    arrange(id, name, telno, email)

usr3a <- usr3 %>%
    select(id, name, telno, email, src) %>%
    arrange(id, name, telno, email)

nr2 <- nrow(usr2)
nr3 <- nrow(usr3)

## 일단 usr2에서 서칭
usr2b <- usr2a %>%
    filter(usr2a$id %in% usr3$id)

nr2b <- nrow(usr2b)

idxMatch <- fMakePureDF(0, nr2b, 1)

for(i in 1:nr2b){

    fDispLoof(i,100,nr2b)
    
    for(j in 1:nr3){
        
        if(usr2b$id[i] == usr3$id[j]){

            if(usr2b$name[i] == usr3$name[j]){
            
                idxMatch[i,1] <- 1
                print(paste(usr2b$id[i], " ", usr3$id[j], " ", usr2b$name[i], " ", usr3$name[j]))
                break()

            }
        }
    }
}
    
usr2c <- usr2b[idxMatch == 1, ]

## usr3에서 서칭
usr3b <- usr3a %>%
    filter(usr3a$id %in% usr2$id)

nr3b <- nrow(usr3b)

idxMatch <- fMakePureDF(0, nr3b, 1)
for(i in 1:nr3b){

    fDispLoof(i, 100, nr3b)

    for(j in 1:nr2){

        if(usr3b$id[i] == usr2$id[j]){

            if(usr3b$name[i] == usr2$name[j]){

                idxMatch[i,1] <- 1
                break()
            }
        }
    }
}

usr3c <- usr3b[idxMatch == 1, ]        

## compare(usr2c, usr3c, allowAll = TRUE)
## FALSE [TRUE, TRUE, FALSE, FALSE]
##   shortened comparison
##   [telno] ignored case
##   [email] ignored case
##   renamed rows
##   [telno] ignored case
##   [email] ignored case
##   dropped row names
##   [telno] ignored case
##   [email] ignored case
## id만 동일, 즉 2,3차가 같은 아이디인데, 사람이 다름


## 두개를 합치기 
names(usr2c) <- c("id","name", "telno2", "email2")
names(usr3c) <- c("id","name", "telno3", "email3", "src")

usr23merge <- full_join(usr2c, usr3c)
fWritePureUtf8Tbl(usr23merge, "../output/usr23merge.csv")

##############################
## 아이디는 같은데 이름이 다른 경우
##

## 일단 usr2에서 서칭

usr2b <- usr2a %>%
    filter(usr2a$id %in% usr3$id)

nr2b <- nrow(usr2b)

idxMatch <- fMakePureDF(0, nr2b, 1)
for(i in 1:nr2b){

    fDispLoof(i,100,nr2b)
    
    for(j in 1:nr3){
        
        if(usr2b$id[i] == usr3$id[j]){

            if(usr2b$name[i] != usr3$name[j]){
            
                idxMatch[i,1] <- 1
                break()

            }
        }
    }
}

usr2c <- usr2b[idxMatch == 1, ]

## usr3에서 서칭
usr3b <- usr3a %>%
    filter(usr3a$id %in% usr2$id)

nr3b <- nrow(usr3b)

idxMatch <- fMakePureDF(0, nr3b, 1)
for(i in 1:nr3b){

    fDispLoof(i, 100, nr3b)

    for(j in 1:nr2){

        if(usr3b$id[i] == usr2$id[j]){

            if(usr3b$name[i] != usr2$name[j]){

                idxMatch[i,1] <- 1
                break()
            }
        }
    }
}

usr3c <- usr3b[idxMatch == 1, ]        

##############################
## 이메일로 찾기

## 3차의 이메일이 2차에 있는 경우
rlt <- usr3[usr3$email %in% usr2$email, ]
## NA 제거
usr32email  <- rlt %>%
    select(email,id, name, src) %>%
    filter(nchar(email) > 1) %>%
    arrange(email)

fWritePureUtf8Tbl(usr32email, "../output/usr32email.csv")

## dim(usr32email)
## [1] 597   4

## head(usr32email)
##                  email         id   name        src
## 1      123@hanmail.net in13041401 우팔명 내부사용자
## 2        123@naver.com in13040892 장영도 내부사용자
## 3        1577@korea.kr in13041642 신형찬 내부사용자
## 4 31080491@hanmail.net   gaja9034 김범석     민원인
## 5        4224@korea.kr in13041655 김태우 내부사용자
## 6      4eva4u@korea.kr in13040205 한재식 내부사용자

##############################
## 온나라 ID로 중복 찾기

## 아이디가 inl로 시작하면 아이디의 2,3차 중복을 찾음
## 우선 각각 아이디에 대해서 inl숫자7자리로 시작하는 것들을 추림

usr3b <- usr3a %>%
    filter(grepl("^in[0-9]{8}$", id))

usr2b <- usr2a %>%
    filter(grepl("^in[0-9]{8}$", id))

usr2a$id
