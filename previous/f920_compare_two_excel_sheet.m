
inputFileName1 = 'input\1.테이블정의서(내부)_180114b_koo.xlsx';
inputFileName2 = 'input\1.테이블정의서(내부)_180115a_lsb.xlsx';
inputSheetName1 = 'column_list';
inputSheetName2 = 'column_list';
inputRange1 = 'A1:U7417';
inputRange2 = 'A1:U7417';


[~,~,input1] = xlsread(inputFileName1, inputSheetName1, inputRange1);
[~,~,input2] = xlsread(inputFileName2, inputSheetName2, inputRange2);


[nR, nC] = size(input1);

for i = 1:nR
    for j = 1:nC
        v1 = input1{i,j};
        v2 = input2{i,j};
        if(~isnan(v1))
            if(~isequal(v1,v2))
                disp([num2str(i), '_' , num2str(j)])
                error('error')
            end
        end
    end
end


 
