### 해운종합 메타 표준화 검증 함수
### 
### input
###  1. 2차년도 단어사전
###  2. 3차년도 단어사전
###  3. TOBE모델 컬럼 논리/물리명
###
### output
###  1. 단어사전 매칭 결과
###
### Algorithm
###  1. 컬럼물리명을 언더바 기준으로 물리명 약어로 분리한다.
###  2. 각 물리명 약어에 해당하는 논리명 약어를 2,3차 사전에서 찾는다.
###  3. 없으면 없는 약어명을 추가하라고 프린트 아웃하고 다음 컬렴물리명으로 진행 
###  4. 있으면 컬럼논리명의 처음부터 그 논리명 약어가 매칭이 되는지를 검토
###  5. 매칭이 되면 매칭되는 부분을 삭제하고 컬럼 논리명을 저장
###  6. 다음 컬럼 물리명 약어에 대해서 동일하게 진행한다.
###  7. 컬럼논리명이 매칭이 안되면, 단어 사전의 컬럼 물리 약어와 현재의 (남은) 컬럼 논리명 및 약어리스트를 
###     디스플레이하고 종료


print('f412 해운종합 컬럼명 메타 표준화 ' )

rm(list=ls())
#options(error = recover)
source("f001_common_lib.R", encoding='UTF-8')
source("f002_common_fn.R", encoding='UTF-8')
source("f003_common_var.R", encoding='UTF-8')

input_meta_word2_nm <- 'input/meta_word2.csv'
input_meta_word3_nm <- 'input/meta_word3.csv'
input_meta_term2_nm <- 'input/meta_term2.csv'

outputName <- '../output/o412_meta_matched.csv'

appColUse_df <- subset(app_col, subset=(col_use != 0))
metaword_2nd <- fReadPureUtf8Tbl(input_meta_word2_nm)
metaword_3nd <- fReadPureUtf8Tbl(input_meta_word3_nm)
metaword <- rbind(metaword_2nd, metaword_3nd)

## 사용하는 메타만 선별
metaword <- subset(metaword, subset = (use == 1))

metaterm_2nd <- fReadPureUtf8Tbl(input_meta_term2_nm)

nC <- nrow(appColUse_df) # 컬럼개수
nword <- nrow(metaword)
nterm <- nrow(metaterm_2nd)

## 매칭이 된 결과를 저장

metaMatched <- fMakePureDF('',100000,2)

idx <- 0
for( i in 1:nC){
  
  colNameLog <- appColUse_df$col_nm_tl[i]
  colNamePig <- appColUse_df$col_nm_tp[i]
  appColNo <- appColUse_df$no[i]
  
  # 컬럼 물리명 분할 개수 찾기 
  
  abbrNum <- str_count(colNamePig,'_') + 1
  
  if(abbrNum == 1){# 컬럼물리명이 단일 단어인 경우
    
    
    abbrNmTp <- colNamePig
    
    
    rlt <- subset(metaword, select = c(nm_log, nm_pig), subset = ( nm_pig == abbrNmTp))
    
    # 매칭이 된 경우, 컬럼 논리명과 사전 논리명이 같은지 확인
    if(nrow(rlt) ==  1){
      
      abbrNmTl <- rlt[1,1]
      
      if(abbrNmTl == colNameLog){
        
        idx <- idx + 1  
        metaMatched[idx,1] <- abbrNmTl
        metaMatched[idx,2] <- abbrNmTp
        
      }else if(nrow(rlt) > 1){
        
        print('약어 중복')
        print(rlt)
        
      }else{
        
        print( paste('no', appColNo, colNameLog, colNamePig))
        print(paste(appColNo, '-' , abbrNmTp))
        stop('no word')
        
      }
    }
  }
  
  
  if(abbrNum > 1){ #컬럼물리명이 복합어인 경우(하이픈으로 연결된)
    for(j in 1:abbrNum){
      
      if(j != abbrNum){
        # 단어 길이
        colPigLen <- nchar(colNamePig)
        colLogLen <- nchar(colNameLog)
        
        # 언버바가 나타나는 지점
        location_unerbar <- str_locate(colNamePig, '_')[1.1]
        
        # 물리 약어 추출    
        abbrNmTp <- substr(colNamePig, 1, (location_unerbar-1)) # 추출부
        colNamePig <- substr(colNamePig, (location_unerbar+1), colPigLen) # 남은 것
        
        ## 논리명이 있는지 사전에서 검색
        rlt <- subset(metaword, select = c(nm_log, nm_pig), subset = ( nm_pig == abbrNmTp))
        
        ## 매칭이 된 경우, 컬럼논리명의 처음에 같은 이름이 있는지 확인한다. 
        if(nrow(rlt) == 1){
          
          abbrNmTl <- rlt[1,1] #사전의 논리명
          idx <- idx + 1
          metaMatched[idx,1] <- abbrNmTl
          metaMatched[idx,2] <- abbrNmTp
          
          
          ## 컬럼논리명의 처음이 이 사전의 논리명으로 시작하는지를 확인(매칭확인)
          rlt2 <- str_locate(colNameLog, abbrNmTl)
          startPnt <- rlt2[1,1]
          endPnt <- rlt2[1,2]
          
          if(!is.na(startPnt)){#매칭이 되면
            
            
            colNameLog <- substr(colNameLog, (endPnt+1), colLogLen)
            
            ## 컬럼논리명의 처음에서 매칭된 사전논리명을 제외하고 나머지를 컬럼논리명으로 대체 
            
          }else{ #매칭이 안되면
            
            print( paste('no', appColNo, abbrNmTl, abbrNmTp, colNameLog  ) )
            # 사전에 단어를 추가할 지 아니면 컬럼명을 표준화할지를 조치를 취할 것
            
            
          }
          
        }else if(nrow(rlt) == 0){
          
          print( paste('no', appColNo, colNameLog, colNamePig  ) )
          print(paste(appColNo, '-' , abbrNmTp))
          stop('no word')
          
        }else{
          
          print('약어 중복')
          print(rlt)
          stop('')
          
        }
      }
    }
  }
}
metaMatched <- metaMatched[1:idx,]

fWritePureUtf8Tbl(metaMatched,outputName)


