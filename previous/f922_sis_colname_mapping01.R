rm(list=ls())
source('z01_common_lib.R')
input_filename_db <- "input/db_sis_naming_db.csv" 
input_filename_doc <- "input/db_sis_naming_doc.csv" 
input_db <- read.csv(input_filename_db, sep=",", fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)
input_doc <- read.csv(input_filename_doc, sep=",", fileEncoding="UTF-8",stringsAsFactors=F,strip.white=TRUE)

nr_db <-nrow(input_db)
nr_doc <- nrow(input_doc)

# 결과를 넣을 데이터 테이블 생성
rltTbl <- data.frame(matrix(NA, nrow = nr_db, ncol = 1))
names(rltTbl) <- 'mapping_colname'
idxmatch = 0
for(i in 1:nr_db){
    fDispLoof(i,100,nr_db)
    dbname_db <-input_db[i,1]
    colname_db <- input_db[i,3]
    for(j in 1:nr_doc){
        dbname_doc <- input_doc[j,1]
        colname_doc <- input_doc[j,3]
        if(dbname_db == dbname_doc ){
            if(colname_db == colname_doc){
                idxmatch = idxmatch +1
                rltTbl[i,1] <- input_doc[j,2]
                break
            }
        }
    }
}

output <- cbind(input_db, rltTbl)
write.csv(output,file='output/x01_colname_mapping.csv')

print(idxmatch)