# a0i5ls1 base analysis
# DB 별로 유저와, 유저-테이블-데이터 수 를 산출함.
# 출력물
# output/a0101_[DB명]_[유저].csv
# oututp/a0102_[DB명]_[유저]_[테이블]_데이터수.csv
# cleaning up environment
# 
# source('C:/00sink/Dropbox (zenesis)/01airsync/b00_e서버/b01_ework/171114pHT_DB_anal/a01_base_anal_ptold01.R', encoding = 'UTF-8')
#
rm(list=ls())
#options(error = recover)
ptm <- proc.time()
source("a00_all_anal_script.R", encoding='UTF-8')

# select server
serverIP = 20
if(serverIP == 18){
    id <- 'DEVDBA'
    pw <- 'deMof$17ba'
    sid <- 'PT_OLD'
    port <- '1521'
    ip <- '192.168.0.18'
} else if(serverIP == 20){
    id <- 'DEVDBA'
    pw <- 'DEVDBA'
    sid <- 'PMIS_SIS'
    ip <- '192.168.0.20'
    port <- '1521'
}

conn <- fDBconnect(ip,port,sid,id,pw)
### 전처리
## 기존 해석 결과 지우기
sysName <- Sys.info()['sysname']
if(sysName == 'Linux'){
    system('rm -rf output/a0101_freq_anal/*')
    system('rm -rf output/a0101_all_table_csv/*')
}


## a0101_DB 별 유저 산출
queryResult<- fDBgetQuery(conn,"SELECT * FROM all_tables")

# count of tabels of PT_OLD
countTbl <- nrow(queryResult)
countCol <- ncol(queryResult)
colNames <- colnames(queryResult)
ownersDup <- queryResult[,1]
owners <- as.data.frame(unique(ownersDup))
outputFileName <- paste("output/a0101_",sid,'_User_analysys_report.csv')
write.csv(owners,file=outputFileName)
ownerAndTbl <- queryResult[,1:2]
fCheckDupRaw(ownerAndTbl)


##a0102_테이블 및 데이터 카운트 리포트

# 모든 오너-테이블에 대해서 컬럼명 불러오기
# DB 명이 시트가 되고 오너-테이블 명이 첫줄에 오고 따라서 컬럼명이 오는 형식으로 정리
nR <- nrow(ownerAndTbl)
idx <- 0
for( i in 1:nR){
    id <-ownerAndTbl[i,1]
    tblName <-str_trim(ownerAndTbl[i,2])
    pw <- fGetPw(sid,id,userTbl)
    pw <- as.character(pw)
    # 불필요한 계정은 패스
    if(id == 'SYS') next
    if(id == 'SYSTEM') next
    if(id=='SCOTT') next
    if(id=='MDSYS') next
    if(id=='LBACSYS') next
    if(id=='DVSYS') next
    if(id=='CTXSYS') next
    if(id=='OJVMSYS') next
    if(id=='WMSYS') next
    if(id=='XDB') next
    if(id=='GSMADMIN_INTERNAL') next
    if(id=='APPQOSSYS') next
    if(id=='OUTLN') next
    if(id=='APEX_040200') next
    if(id=='AUDSYS') next
    if(id=='ORDSYS') next
    if(id=='ORDDATA') next
    if(id=='FLOWS_FILES') next
    if(id=='OLAPSYS') next
    if(tblName == 'PTB_SSOP7103') next
    if(tblName == 'PTB_SSOP3201') next
    if(tblName == '1150 PTB_SSOP0006_H') next
    


    if(length(pw)==0) {
        print(paste(sid,'-',id,'has no paswd')) 
    } else {
        idx <- idx + 1
        
        Sys.sleep(20) # DB 부하 방지를 위해서 5초간 휴식
        
        if(serverIP==18){
        #if(id=='OUTLN') next #lock
        #if(id=='GSMADMIN_INTERNAL') next # ORA-01045 error
        #if(id=='OJVMSYS') next # ORA-01045 error
        #if(id=='ORDDATA') next #  ORA-28000 error
        #if(id=='APEX_040200') next #  ORA-28000 error
        #if(id=='AUDSYS') next #  ORA-46370 error
        #if(id=='ORDSYS') next #  ORA-28000 error
        #if(id=='OLAPSYS') next #  ORA-28000 error
        #if(id=='FLOWS_FILES') next #  ORA-28000 error
        }else if(serverIP==20){
        if(tblName == 'MBBS_ORG') next #BLOB
        if(tblName == 'MBRP_DECL_D') next
        if(tblName == 'MBTX_NAV_VESSEL') next
        if(tblName == 'MBTX_VESSEL_STD') next
        if(tblName == 'TC_ORG') next
        if(tblName == 'MBTX_TONSE_INCOME') next
        
        }
        
        print(paste('user::', id,'-table::',(idx+1000),tblName,'start' ))
        conn <- fDBconnect(ip,port,sid,id,pw)
        querySentence <- paste( 'select * from', tblName)

        ## 테이블 정보가 제대로 왔는지 확인 필요 
        getQuery <- fDBgetQuery(conn, querySentence)
        dfTbl <- tbl_df(getQuery) #data.table packages
        ## 테이블 저장(잠시 중단.. 부하문제)
        #outputName <- paste('output/a0101_all_table_csv/',(idx+1000),'_',tblName,'.csv',sep="")
        #write.csv(dfTbl,outputName,row.names=FALSE)
        dfTblDataCount <- nrow(dfTbl)
        dfTblColCount <- ncol(dfTbl)
        if(dfTblDataCount == 0) next
        dfTblColName <- colnames(dfTbl)
        idxDF <- data.frame(idxFreq = c(1:dfTblDataCount))
        
        ## 컬럼별로 빈도를 확인해서 최대 빈도수를 추출
        maxFreq = 0
        for(j in 1:dfTblColCount){
            colFreq_dec <- as.data.frame(sort(table(dfTbl[ ,dfTblColName[j]]),decreasing=T))
            if(maxFreq < nrow(colFreq_dec)) maxFreq <- nrow(colFreq_dec)
        }

        outputTbl <- fMakePureDF('-', maxFreq, dfTblColCount * 4 )         

        ## 퀄럼별로 빈도추출 
        for(j in 1:dfTblColCount){
            print(paste(j,'/', dfTblColCount))
            colFreq_dec <- as.data.frame(sort(table(dfTbl[ ,dfTblColName[j]]),decreasing=T), stringsAsFactors=FALSE)
            colFreq_inc <- as.data.frame(sort(table(dfTbl[ ,dfTblColName[j]]),decreasing=F), stringsAsFactors=FALSE)
            eachColName1 <- c(paste(str_trim(dfTblColName[j]),'_d',sep=""), paste(str_trim(dfTblColName[j]),'_df',sep=""))
            eachColName2 <- c(paste(str_trim(dfTblColName[j]),'_i',sep=""), paste(str_trim(dfTblColName[j]),'_if',sep=""))

            # if(nrow(colFreq_dec)== 2 ) browser()
            ## 빈도 집합의 요소수가 2이상의 정상적인 경우에는 아래와 같이 나옴
            ##
            ##     var1   Freq
            ##  1  1      1144
            ##  2  2      698
            ##

            ## 빈도 집합의 요소수가 0인 경우 즉 데이터가 0인 경우에 대한 예외 처리
            ##if(nrow(colFreq_dec)== 0 ) colFreq_dec <- data.frame(matrix('-',nrow=1,ncol=2))
            ## if(nrow(colFreq_inc)== 0 ) colFreq_inc <- data.frame(matrix('-',nrow=1,ncol=2))
            if(nrow(colFreq_dec)==0){
                colFreq <- data.frame(matrix('-',nrow=1,ncol=4))
                names(colFreq) <- c(eachColName1, eachColName2)
            }
            ## 빈도 집합의 요소 수가 1인 경우에도 에러가 발생한다. 이에 대해서도 처리가 필요함
            ## 변수명이 row name이 되고, 컬럼명이 이상하게 나옴
            ##   
            ##          이상한 컬럼명
            ##  변수명  빈도값 
            ##  
            else if(ncol(colFreq_dec)==1){
                colFreq <- data.frame(matrix(NA, nrow=1, ncol=4))
                rowName <- rownames(colFreq_dec)
                names(colFreq) <- c(paste(rowName,'_d',sep=""),paste(rowName,'_df',sep=""),paste(rowName,'_i',sep=""),paste(rowName,'_if',sep=""))
                colFreq[1] <- rowName
                colFreq[2] <- colFreq_dec[1,1]
                colFreq[3] <- rowName
                colFreq[4] <- colFreq_dec[1,1]
            }
            ## 빈도 집합 요소 수가 2로서 정상인 경우
            else if(ncol(colFreq_dec)==2){ 
                names(colFreq_dec) <- eachColName1
                names(colFreq_inc) <- eachColName2
                colFreq <- cbind(colFreq_dec,colFreq_inc)
                colFreqCount <-nrow(colFreq)

            }else{
                stop('error')
            }

            
            
            ## 컬럼명 변경
            startPoint <- (j-1)*4 +1
            endPoint <- j * 4
            names(outputTbl)[startPoint:endPoint] <- names(colFreq)
            
            ## 값 넣기
            col1 <- (j-1)*4 +1 
            col2 <- j * 4
            row1 <- 1
            row2 <- colFreqCount
            outputTbl[row1:row2, col1:col2] <- colFreq
        }
        dbDisconnect(conn)
        outputTblFileName <- paste('output/a0101_freq_anal/',(idx+1000),'_',tblName,'.csv',sep="")
        write.csv(outputTbl, outputTblFileName, row.names=FALSE)
    }     
}
print("a01 succedd")
fDispCalTime(ptm)



